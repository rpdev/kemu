require 'lib/EmulatorModel'

module KEmu

    class VirtualMachineConfigurationDialog < Qt::Dialog

        def initialize( virtualMachine, parent )
            super( parent )
            
            @virtualMachine = virtualMachine

            createGui
            setupGui
        end

        private

        def createGui
            @nameWidget = Qt::Widget.new( self )
            @nameWidget.layout = Qt::HBoxLayout.new
            @nameLabel = Qt::Label.new( i18n( "Name:" ), @nameWidget )
            @nameEdit = Qt::LineEdit.new( @nameWidget )
            @nameEdit.text = @virtualMachine.name
            @nameWidget.layout.addWidget( @nameLabel )
            @nameWidget.layout.addWidget( @nameEdit )

            @emulatorList = Qt::TreeView.new( @emulatorWidget )
            @emulatorList.model = EmulatorModel.new( self )
            
            Emulator.emulators.length.times do | index |
                if Emulator.emulators[ index ].name == @virtualMachine.emulator.class.name then
                    @emulatorList.setCurrentIndex( @emulatorList.model.index( index, 0 ) )
                    break
                end
            end

            @buttons = Qt::DialogButtonBox.new( Qt::DialogButtonBox::Ok | Qt::DialogButtonBox::Cancel,
                                                Qt::Horizontal,
                                                self )
            
            self.layout = Qt::VBoxLayout.new
            self.layout.addWidget( @nameWidget )
            self.layout.addWidget( @emulatorList )
            self.layout.addWidget( @buttons )
        end

        def setupGui
            @buttons.connect( SIGNAL('accepted()') ) { accept }
            @buttons.connect( SIGNAL('accepted()') ) { applySettings }
            @buttons.connect( SIGNAL('rejected()') ) { reject }
        end

        def applySettings
            @virtualMachine.name = @nameEdit.text
            @emulatorClass = Emulator.emulators[ @emulatorList.currentIndex.row ]
            if not @emulatorClass.nil? then
                if @emulatorClass.name != @virtualMachine.emulator.class.name then
                    @virtualMachine.emulator = @emulatorClass.new( @virtualMachine )
                end
            end
        end

    end

end