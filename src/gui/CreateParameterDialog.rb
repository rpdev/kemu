require 'lib/VirtualMachineParameterClassModel'

module KEmu

    class CreateParameterDialog < Qt::Dialog

        def initialize( virtualMachine, parent )
            super( parent )
            @virtualMachine = virtualMachine

            createGui
            setupGui
        end

        def parameterClass
            return VirtualMachineParameter.parameters[ @parameterView.currentIndex.row ]
        end

        private

        def createGui
            self.layout = Qt::VBoxLayout.new

            @parameterView = Qt::TreeView.new
            @parameterView.model = VirtualMachineParameterClassModel.new( self )

            @buttons = Qt::DialogButtonBox.new( Qt::DialogButtonBox::Ok | Qt::DialogButtonBox::Cancel,
                                                Qt::Horizontal,
                                                self )

            self.layout.addWidget( @parameterView )
            self.layout.addWidget( @buttons )
        end

        def setupGui
            @buttons.connect( SIGNAL('accepted()') ) { accept }
            @buttons.connect( SIGNAL('rejected()') ) { reject }
        end

    end

end