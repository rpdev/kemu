require 'gui/CreateParameterDialog'
require 'gui/VirtualMachineConfigurationDialog'

require 'lib/VirtualMachineManager'

module KEmu

    MAINWINDOW_OBJECTNAME = "MainWindow"

    class MainWindow < KDE::MainWindow

        def initialize( parent = nil )
            super( parent )

            createObjects
            createGui
            createActions
            createMenus
            setupGui
            setupMenus
            setupActions
            createTrayIcon

            restoreSettings
        end

        protected

        def queryClose
            @virtualMachineManager.machines.each do | machine |
                if machine.running? then
                    hide()
                    return false
                end
            end
            saveSettings
            return true
        end

        private

        def createObjects
            @virtualMachineManager = VirtualMachineManager.new( self )
        end

        def createGui
            @mainSplitter = Qt::Splitter.new( self )
            @mainSplitter.orientation = Qt::Vertical

            @secondarySplitter = Qt::Splitter.new( self )
            @secondarySplitter.orientation = Qt::Horizontal

            @machineView = Qt::TreeView.new( self )
            @machineView.model = @virtualMachineManager.model
            
            @parameterView = Qt::TreeView.new( self )
            
            @outputView = Qt::TreeView.new( self )

            @secondarySplitter.addWidget( @machineView )
            @secondarySplitter.addWidget( @parameterView )

            @mainSplitter.addWidget( @secondarySplitter )
            @mainSplitter.addWidget( @outputView )

            setCentralWidget( @mainSplitter )
        end

        def setupGui
            self.objectName = MAINWINDOW_OBJECTNAME
            self.windowTitle = i18n( "KEmu Virtual Machine Manager" )
            resize( 800, 600 )
            KDE::Application.instance.connect( SIGNAL('aboutToQuit()') ) { onApplicationQuit }
            @machineView.connect( SIGNAL('clicked(QModelIndex)') ) { | index | onMachineSelected( index ) }
            @machineView.connect( SIGNAL('doubleClicked(QModelIndex)') ) { configureVirtualMachine }
            @parameterView.connect( SIGNAL('doubleClicked(QModelIndex)') ) { configureParameter }
        end

        def createMenus
            @applicationMenu = menuBar.addMenu( i18n( "Application" ) )
            @virtualMachineMenu = menuBar.addMenu( i18n( "Virtual Machines" ) )
            @parameterMenu = menuBar.addMenu( i18n( "Parameters" ) )
            @helpMenu = helpMenu()
            self.menuBar.addMenu( @helpMenu )
        end

        def setupMenus
            @applicationMenu.addAction( @quitAction )

            @virtualMachineMenu.addAction( @runVirtualMachineAction )
            @virtualMachineMenu.addSeparator
            @virtualMachineMenu.addAction( @createVirtualMachineAction )
            @virtualMachineMenu.addAction( @configureVirtualMachineAction )
            @virtualMachineMenu.addAction( @removeVirtualMachineAction )

            @parameterMenu.addAction( @createParameterAction )
            @parameterMenu.addAction( @configureParameterAction )
            @parameterMenu.addAction( @removeParameterAction )
        end

        def createActions
            @quitAction = KDE::Action.new( i18n( "Quit" ), self )
            @quitAction.shortcut = i18n( "Ctrl+Q" )

            @runVirtualMachineAction = KDE::Action.new( i18n( "Run" ), self )
            @createVirtualMachineAction = KDE::Action.new( i18n( "New" ), self )
            @configureVirtualMachineAction = KDE::Action.new( i18n( "Configure" ), self )
            @removeVirtualMachineAction = KDE::Action.new( i18n( "Remove" ), self )

            @createParameterAction = KDE::Action.new( i18n( "New" ), self )
            @configureParameterAction = KDE::Action.new( i18n( "Configure" ), self )
            @removeParameterAction = KDE::Action.new( i18n( "Remove" ), self )
        end

        def setupActions
            @quitAction.connect( SIGNAL('triggered()') ) { KDE::Application.instance.quit }

            @runVirtualMachineAction.connect( SIGNAL('triggered()') ) { runVirtualMachine }
            @createVirtualMachineAction.connect( SIGNAL('triggered()') ) { createVirtualMachine }
            @configureVirtualMachineAction.connect( SIGNAL('triggered()') ) { configureVirtualMachine }
            @removeVirtualMachineAction.connect( SIGNAL('triggered()') ) { removeVirtualMachine }

            @createParameterAction.connect( SIGNAL('triggered()') ) { createParameter }
            @configureParameterAction.connect( SIGNAL('triggered()') ) { configureParameter }
            @removeParameterAction.connect( SIGNAL('triggered()') ) { removeParameter }
        end
            

        def createTrayIcon
            @trayIcon = KDE::StatusNotifierItem.new( self )
            @trayIcon.setIconByName( "kemu" )
            @trayIcon.associatedWidget = self
            @trayIcon.category = KDE::StatusNotifierItem.ApplicationStatus
            @trayIcon.status = KDE::StatusNotifierItem.Active
            @trayIcon.standardActionsEnabled = true
        end

        def onApplicationQuit
        end

        # private slots

        def createVirtualMachine
            vm = VirtualMachine.new( @virtualMachineManager )
            @virtualMachineManager.add( vm )
            if Qt::MessageBox.question( self,
                                        i18n( "Configure Now?" ),
                                        i18n( "Would you like to configure the new virtual machine now? This allows you e.g. to give it an unique name. (Note, that you can configure the machine on any later point, too)" ),
                                        Qt::MessageBox.Yes,
                                        Qt::MessageBox.No ) == Qt::MessageBox.Yes then
                dialog = VirtualMachineConfigurationDialog.new( vm, self )
                dialog.exec
            end
        end

        def configureVirtualMachine
            if not @machineView.currentIndex.nil? then
                vm = @virtualMachineManager.machines[ @machineView.currentIndex.row ]
                if not vm.nil? then
                    dialog = VirtualMachineConfigurationDialog.new( vm, self )
                    dialog.exec
                end
            end
        end

        def removeVirtualMachine
            vm = @virtualMachineManager.machines[ @machineView.currentIndex.row ]
            if not vm.nil? then
                if Qt::MessageBox.question( self,
                                            i18n( "Remove Virtual Machine?" ),
                                            i18n( "Do you really want to remove the selected virtual machine? This cannot be undone later!" ),
                                            Qt::MessageBox.Yes,
                                            Qt::MessageBox.No ) == Qt::MessageBox.Yes then
                    if @parameterView.model == vm.parameterModel then
                        @parameterView.model = nil
                    end
                    @virtualMachineManager.remove( vm )
                end
            end
        end

        def runVirtualMachine
            vm = @virtualMachineManager.machines[ @machineView.currentIndex.row ]
            if not vm.nil? then
                if not vm.running? then
                    if not vm.run then
                        if Qt::MessageBox.warning( self, i18n( "Failed to Start Machine" ),
                                                i18n( "Unable to start the virtual machine #{vm.name}. Probably no emulator has been set. Please select one in the machine's configuration dialog. Do you want to select one now?" ),
                                                Qt::MessageBox.Yes, Qt::MessageBox.No ) == Qt::MessageBox.Yes then
                            configureVirtualMachine
                        end
                    end
                end
            end
        end

        def createParameter
            if not @parameterView.model.nil? then
                dialog = CreateParameterDialog.new( @parameterView.model.virtualMachine, self )
                if dialog.exec == Qt::Dialog::Accepted then
                    parameterClass = dialog.parameterClass
                    if not parameterClass.nil? then
                        if parameterClass.unique? then
                            if @parameterView.model.virtualMachine.parametersOfClass( parameterClass.name ).length > 0 then
                                Qt::MessageBox.warning( self,
                                                        i18n( "Parameter Already Present" ),
                                                        i18n( "The parameter you try to add is unique, i.e. you can create only one per virtual machine." )
                                                      )
                                return
                            end
                        end
                        parameter = parameterClass.new( @parameterView.model.virtualMachine )
                        @parameterView.model.addParameter( parameter )
                    end
                end
            end
        end

        def configureParameter
            if not @parameterView.model.nil? then
                vm = @parameterView.model.virtualMachine
                parameter = vm.parameters[ @parameterView.currentIndex.row ]
                if not parameter.nil? then
                    if parameter.configurable? then
                        if not vm.emulator.nil? then
                            if vm.emulator.hasSpecialEditorForParameter?( parameter.class.name ) then
                                vm.emulator.editParameter( parameter )
                                return
                            end
                        end
                        parameter.configure
                    end
                end
            end
        end

        def removeParameter
            if not @parameterView.model.nil? then
                if not @parameterView.model.virtualMachine.parameters[ @parameterView.currentIndex.row ].nil? then
                    if Qt::MessageBox.question( self,
                                                i18n( "Remove Parameter" ),
                                                i18n( "Do you really want to remove the selected parameter from the machine? This cannot be undone!" ),
                                                Qt::MessageBox.Yes,
                                                Qt::MessageBox.No ) == Qt::MessageBox.Yes then
                        @parameterView.model.removeParameter( @parameterView.currentIndex )
                    end
                end
            end
        end

        def onMachineSelected( index )
            vm = @virtualMachineManager.machines[ index.row ]
            if not vm.nil? then
                @parameterView.model = vm.parameterModel
            else
                @parameterView.model = nil
            end
        end

        SETTINGS_GROUP_VIRTUALMACHINEMANAGER      = "VirtualMachineManager"
        SETTINGS_KEY_VIRTUALMACHINEMANAGER      = "Configuration"

        SETTINGS_GROUP_GUI                      = "GUI"
        SETTINGS_KEY_PRIMARYSPLITTER            = "MainSplitterState"
        SETTINGS_KEY_SECONDARYPLITTER           = "SecondarySplitterState"
        SETTINGS_KEY_VIRTUALMACHINEVIEWHEADER   = "VirtualMachineViewHeader"
        SETTINGS_KEY_MAINWINDOWSTATE            = "MainWindowState"
        SETTINGS_KEY_MAINWINDOWGEOMETRY         = "MainWindowGeometry"
        
        def saveSettings
            puts "Saving settings..."
            
            config = KDE::Global.config.group( SETTINGS_GROUP_VIRTUALMACHINEMANAGER )
            config.writeEntry( SETTINGS_KEY_VIRTUALMACHINEMANAGER, Qt::Variant.fromValue( @virtualMachineManager.saveSettings ) )
            config.sync

            config = KDE::Global.config.group( SETTINGS_GROUP_GUI )
            config.writeEntry( SETTINGS_KEY_PRIMARYSPLITTER, Qt::Variant.fromValue( @mainSplitter.saveState ) )
            config.writeEntry( SETTINGS_KEY_SECONDARYPLITTER, Qt::Variant.fromValue( @secondarySplitter.saveState ) )
            config.writeEntry( SETTINGS_KEY_VIRTUALMACHINEVIEWHEADER, Qt::Variant.fromValue( @machineView.header.saveState ) )
            config.writeEntry( SETTINGS_KEY_MAINWINDOWSTATE, Qt::Variant.fromValue( saveState() ) )
            config.writeEntry( SETTINGS_KEY_MAINWINDOWGEOMETRY, Qt::Variant.fromValue( saveGeometry() ) )
            config.sync
            
            puts "Settings successfully saved!"
        end

        def restoreSettings
            puts "Restoring settings..."
            
            config = KDE::Global.config.group( SETTINGS_GROUP_VIRTUALMACHINEMANAGER )
            data = config.readEntry( SETTINGS_KEY_VIRTUALMACHINEMANAGER, Qt::Variant.fromValue( Qt::ByteArray.new ) ).toByteArray
            if not data.empty? then
                @virtualMachineManager.restoreSettings( data )
            end

            config = KDE::Global.config.group( SETTINGS_GROUP_GUI )
            @mainSplitter.restoreState( config.readEntry( SETTINGS_KEY_PRIMARYSPLITTER, Qt::Variant.fromValue( Qt::ByteArray.new ) ).toByteArray )
            @secondarySplitter.restoreState( config.readEntry( SETTINGS_KEY_SECONDARYPLITTER, Qt::Variant.fromValue( Qt::ByteArray.new ) ).toByteArray )
            @machineView.header.restoreState( config.readEntry( SETTINGS_KEY_VIRTUALMACHINEVIEWHEADER, Qt::Variant.fromValue( Qt::ByteArray.new ) ).toByteArray )
            restoreState( config.readEntry( SETTINGS_KEY_MAINWINDOWSTATE, Qt::Variant.fromValue( Qt::ByteArray.new ) ).toByteArray )
            restoreGeometry( config.readEntry( SETTINGS_KEY_MAINWINDOWGEOMETRY, Qt::Variant.fromValue( Qt::ByteArray.new ) ).toByteArray )
            
            puts "Settings successfully restored!"
        end

    end

end