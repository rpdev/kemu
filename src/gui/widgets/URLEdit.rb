module KEmu

    # Provides widget classes that are used multiple times.
    #
    # This module provides some composited widgets (i.e.
    # widgets that consist of several other widgets) to provide
    # additional functionality or simply avoid coding overhead.
    module Widgets

        # Widget to edit (file) URLs.
        #
        # This composited widget consists of an edit field and an additional
        # button the user can use to select files or directories via an
        # special dialog.
        class URLEdit < Qt::Widget

            # Determines, whether the file dialog let the user select files or
            # directories.
            #
            # By default, this is set to true, meaning the dialog is used to
            # select files. When setting to false, the dialog will let
            # the user select directories.
            attr_accessor :selectFiles

            # Constructor.
            #
            # The parent parameter is the parent widget to which the widget
            # will be added.
            def initialize( parent = nil )
                super( parent )
                
                @selectFiles = true

                createGui
                setupGui
            end

            # The text in the edit.
            def text
                return @lineEdit.text
            end

            # Sets the URL. This will set the text in the edit field.
            def text=( newUrl )
                @lineEdit.text = newUrl
            end

            private

            def createGui
               @lineEdit = Qt::LineEdit.new( self )

               @button = Qt::ToolButton.new( self )
               @button.text = i18n( "..." )

               self.layout = Qt::HBoxLayout.new
               self.layout.addWidget( @lineEdit )
               self.layout.addWidget( @button )
            end

            def setupGui
                @button.connect( SIGNAL('clicked()') ) { selectUrl }
            end

            def selectUrl
                file = @lineEdit.text
                if @selectFiles then
                    file = Qt::FileDialog.getOpenFileName( self, i18n( "Select File" ), file )
                else
                    file = Qt::FileDialog.getExistingDirectory( self, i18n( "Select File" ), file )
                end
                if not file.nil? and not file.empty? then
                    @lineEdit.text = file
                end
            end

        end

    end

end