###########################################################################
#    (c) 2009 Martin Höher (martin@rpdev.net)                             #
#                                                                         #
#    This file is part of KEmu.                                           #
#                                                                         #
#    KEmu is free software: you can redistribute it and/or modify         #
#    it under the terms of the GNU General Public License as published by #
#    the Free Software Foundation, either version 3 of the License, or    #
#    (at your option) any later version.                                  #
#                                                                         #
#    KEmu is distributed in the hope that it will be useful,              #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        #
#    GNU General Public License for more details.                         #
#                                                                         #
#    You should have received a copy of the GNU General Public License    #
#    along with KEmu.  If not, see <http://www.gnu.org/licenses/>.        #
###########################################################################

require 'Qt4'
require 'korundum4'

require 'adapters/adapterlist'

require 'ui/ui_setup_general'
require 'ui/ui_setup_drives'
require 'ui/ui_setup_target'
require 'ui/ui_setup_network'

###################################################################
# Configuration form class.
# This class visually represents the settings of a single
# virtual machine. It provides also the functionality to
# get and set the appropriate fields.
###################################################################
class ConfigForm < KDE::PageDialog

  ###################################################################
  # Constructor.
  # Sets up the form and initializes static contents.
  ###################################################################
  def initialize(*k)
    super
    setupUi
    setupActions
    fillStaticLists
    resize(500, 300)
  end

  ###################################################################
  # Set up the UI.
  # This internally creates the UI.
  ###################################################################
  def setupUi
    @generalWidget = Qt::Widget.new
    @generalUi = Ui::SetupGeneralPage.new
    @generalUi.setupUi(@generalWidget)
    @generalPage = self.addPage(@generalWidget, i18n("General"))
    @generalPage.setIcon(KDE::Icon.new("preferences-other"))

    @drivesWidget = Qt::Widget.new
    @drivesUi = Ui::SetupDrivesPage.new
    @drivesUi.setupUi(@drivesWidget)
    @drivesPage = self.addPage(@drivesWidget, i18n("Drives"))
    @drivesPage.setIcon(KDE::Icon.new("drive-harddisk"))

    @targetWidget = Qt::Widget.new
    @targetUi = Ui::SetupTargetPage.new
    @targetUi.setupUi(@targetWidget)
    @targetPage = self.addPage(@targetWidget, i18n("Target"))
    @targetPage.setIcon(KDE::Icon.new("cpu"))

    @networkWidget = Qt::Widget.new
    @networkUi = Ui::SetupNetworkPage.new
    @networkUi.setupUi(@networkWidget)
    @networkPage = self.addPage(@networkWidget, i18n("Network"))
    @networkPage.setIcon(KDE::Icon.new("network"))
    @networkUi.tftp.mode = KDE::File.Directory
  end

  ###################################################################
  # Set up actions.
  # This connects internally used signals and slots.
  ###################################################################
  def setupActions
    @targetUi.target.connect(SIGNAL('currentIndexChanged(int)')) { updateMachineList }
  end

  ###################################################################
  # Fill static list contents.
  # This fills lists having static contents.
  ###################################################################
  def fillStaticLists
    fillKeyboardLayoutList
    fillEmulatorList
    fillBootOnList
    fillVgaCardList
  end

  ###################################################################
  # Fill in keyboard layouts.
  # This method fills the list of keyboard layouts.
  ###################################################################
  def fillKeyboardLayoutList
    @targetUi.kblayout.clear
    for item in ["", "ar", "da", "de", "de-ch", "en-gb", "en-us", "es",
                 "et", "fi", "fo", "fr", "fr-be", "fr-ca", "fr-ch", "hr",
                 "hu", "is", "it", "ja", "lt", "lv", "mk", "nl", "nl-be",
                 "no", "pl", "pt", "pt-br", "ru", "sl", "sv", "th", "tr"] do
      @targetUi.kblayout.addItem(item)
    end
  end

  ###################################################################
  # Fill the list of emulators.
  # This fills the list with all known qemu-system-* executables and
  # qemu-kvm.
  ###################################################################
  def fillEmulatorList
    @targetUi.target.clear
    for item in AdapterList.instance.adapterDisplayList do
      @targetUi.target.addItem(item)
    end
  end

  ###################################################################
  # Fill the list with boot targets.
  # This fills the list where the user can select where to boot
  # from.
  ###################################################################
  def fillBootOnList
    @bootOn_items = { "" => i18n("Default"),
                      "a" => i18n("Floppy"),
                      "c" => i18n("Hard Disk"),
                      "d" => i18n("CD-ROM"),
                      "n" => i18n("Network")}
    @targetUi.bootOn.clear
    for item in @bootOn_items.values do
      @targetUi.bootOn.addItem(item)
    end
  end

  ###################################################################
  # Fill the list if VGA card.
  # Fill in the list with all available VGA cards.
  ###################################################################
  def fillVgaCardList
    @targetUi.vgaCard.clear
    for item in ["", "cirrus", "std", "vmware", "none"] do
      @targetUi.vgaCard.addItem(item)
    end
  end

  ###################################################################
  # Updates auto-generated list contents.
  # This fills list content dependent on the selected emulator.
  # The filles lists are read from command line output of the given
  # program instead of statically embedding them in the program.
  ###################################################################
  def updateMachineList
    adapter = AdapterList.instance.findAdapter(self.target)
    machines = [""]
    cpus = [""]
    soundhws = [""]
    if adapter != nil then
        machines = adapter.machineList
        cpus = adapter.cpuList
        soundhws = adapter.soundHwList
    end
    
    @targetUi.machine.clear
    for machine in machines do
      @targetUi.machine.addItem(machine)
    end
    
    @targetUi.cpu.clear
    for cpu in cpus do
      @targetUi.cpu.addItem(cpu)
    end

    @targetUi.soundhw.clear
    for soundhw in soundhws do
      @targetUi.soundhw.addItem(soundhw)
    end
  end

  ###################################################################
  # Sets the current index of drop down list "list", so that
  # the item with text "item" is selected or (if there is no
  # such entry) set index to "defindex", which is usually the
  # first item in the list.
  ###################################################################
  def setListToItem(list, item, defindex = 0)
    list.currentIndex = defindex
    for i in (0..list.count-1) do
      if list.itemText(i) == item then
	list.currentIndex = i
	return
      end
    end
  end

  ###################################################################
  ###################################################################
  #                         GETTER AND SETTER
  ###################################################################
  ###################################################################

  def name
    @generalUi.name.text
  end

  def name=(name)
    @generalUi.name.text = name
  end

  def hda
    @drivesUi.hda.text
  end

  def hda=(hda)
    @drivesUi.hda.text = hda
  end

  def hdb
    @drivesUi.hdb.text
  end

  def hdb=(hdb)
    @drivesUi.hdb.text = hdb
  end

  def hdc
    @drivesUi.hdc.text
  end

  def hdc=(hdc)
    @drivesUi.hdc.text = hdc
  end

  def hdd
    @drivesUi.hdd.text
  end

  def hdd=(hdd)
    @drivesUi.hdd.text = hdd
  end

  def fda
    @drivesUi.fda.text
  end

  def fda=(fda)
    @drivesUi.fda.text = fda
  end

  def fdb
    @drivesUi.fdb.text
  end

  def fdb=(fdb)
    @drivesUi.fdb.text = fdb
  end

  def cdrom
    @drivesUi.cdrom.text
  end

  def cdrom=(cdrom)
    @drivesUi.cdrom.text = cdrom
  end

  def sd
    @drivesUi.sd.text
  end

  def sd=(sd)
    @drivesUi.sd.text = sd
  end

  def obflash
    @drivesUi.obflash.text
  end

  def obflash=(obflash)
    @drivesUi.obflash.text = obflash
  end

  def pflash
    @drivesUi.pflash.text
  end

  def pflash=(pflash)
    @drivesUi.pflash.text = pflash
  end

  def target
    adapter = AdapterList.instance.findAdapter(@targetUi.target.currentText)
    if adapter != nil
        return adapter.executable
    end
    return nil
  end

  def target=(target)
      adapter = AdapterList.instance.findAdapter(target)
      if adapter != nil then
          setListToItem(@targetUi.target, adapter.printedName)
      else
          @targetUi.target.currentIndex = 0
      end
  end

  def machine
    @targetUi.machine.currentText
  end

  def machine=(machine)
    setListToItem(@targetUi.machine, machine)
  end

  def bootOn
    for item in @bootOn_items.keys do
      if @bootOn_items[item] == @targetUi.bootOn.currentText then
	return item
      end
    end
    return ""
  end

  def bootOn=(bootOn)
    @targetUi.bootOn.currentIndex = 0
    if @bootOn_items.has_key?(bootOn)
      setListToItem(@targetUi.bootOn, @bootOn_items[bootOn])
    end
  end

  def cpu
    @targetUi.cpu.currentText
  end

  def cpu=(cpu)
    setListToItem(@targetUi.cpu, cpu)
  end

  def numCpu
    @targetUi.numCpu.value
  end

  def numCpu=(numCpu)
    @targetUi.numCpu.value = numCpu
  end

  def memory
    @targetUi.memory.value
  end

  def memory=(memory)
    @targetUi.memory.value = memory
  end

  def soundhw
    @targetUi.soundhw.currentText
  end

  def soundhw=(soundhw)
    setListToItem(@targetUi.soundhw, soundhw)
  end

  def allSoundhw
    @targetUi.allSoundhw.checked?
  end

  def allSoundhw=(allSoundhw)
    @targetUi.allSoundhw.checked = allSoundhw
  end

  def usb
    @targetUi.enableUsb.checked?
  end

  def usb=(usb)
    @targetUi.enableUsb.checked = usb
  end

  def vgaCard
    @targetUi.vgaCard.currentText
  end

  def vgaCard=(vgaCard)
    setListToItem(@targetUi.vgaCard, vgaCard)
  end

  def noFrame
    @targetUi.noFrame.checked?
  end

  def noFrame=(noFrame)
    @targetUi.noFrame.checked = noFrame
  end

  def noQuit
    @targetUi.noQuit.checked?
  end

  def noQuit=(noQuit)
    @targetUi.noQuit.checked = noQuit
  end

  def portrait
    @targetUi.portrait.checked?
  end

  def portrait=(portrait)
    @targetUi.portrait.checked = portrait
  end

  def fullScreen
    @targetUi.fullScreen.checked?
  end

  def fullScreen=(fullScreen)
    @targetUi.fullScreen.checked = fullScreen
  end

  def altGrab
    @targetUi.altGrab.checked?
  end

  def altGrab=(altGrab)
    @targetUi.altGrab.checked = noFrame
  end

  def kblayout
    @targetUi.kblayout.currentText
  end

  def kblayout=(kblayout)
    setListToItem(@targetUi.kblayout, kblayout)
  end

  def userNetwork
    @networkUi.userNetwork.checked?
  end

  def userNetwork=(userNetwork)
    @networkUi.userNetwork.checked = userNetwork
  end

  def smb
    @networkUi.smb.text
  end

  def smb=(smb)
    @networkUi.smb.text = smb
  end

  def tftp
    @networkUi.tftp.text
  end

  def tftp=(tftp)
    @networkUi.tftp.text = tftp
  end

end