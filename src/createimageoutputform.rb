###########################################################################
#    (c) 2009 Martin Höher (martin@rpdev.net)                             #
#                                                                         #
#    This file is part of KEmu.                                           #
#                                                                         #
#    KEmu is free software: you can redistribute it and/or modify         #
#    it under the terms of the GNU General Public License as published by #
#    the Free Software Foundation, either version 3 of the License, or    #
#    (at your option) any later version.                                  #
#                                                                         #
#    KEmu is distributed in the hope that it will be useful,              #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        #
#    GNU General Public License for more details.                         #
#                                                                         #
#    You should have received a copy of the GNU General Public License    #
#    along with KEmu.  If not, see <http://www.gnu.org/licenses/>.        #
###########################################################################

require 'korundum4'

require 'ui/ui_createimageoutputform'

class CreateImageOutputForm < Qt::Widget
  
  attr_reader :ui
  attr_accessor :process
  
  
  def initialize(*k)
    super
    
    @process = nil
    @ui = Ui::CreateImageOutputForm.new
    @ui.setupUi(self)
    
    windowModality = Qt::ApplicationModal;
  end
  
  def setupActions
    @process.connect(SIGNAL(:started)) { self.onProcessStarted }
    @process.connect(SIGNAL("finished(int,QProcess::ExitStatus)")) { self.onProcessFinished }
    @process.connect(SIGNAL(:readyReadStandardOutput)) { self.stdOut }
    @process.connect(SIGNAL(:readyReadStandardError)) { self.stdErr }
    @ui.button.connect(SIGNAL(:clicked)) { self.onButtonClick }
  end
  
  def show(*k)
    super
    onShow
  end
  
  def onShow
    @ui.button.text = i18n("Close")
    @process.start
  end
  
  def onProcessStarted
    @ui.output.clear
    @ui.output.append("<span style='color: green'>#{i18n("Started...")}</span>")
    @ui.button.text = i18n("Cancel")
    @ui.progressBar.maximum = 0
  end
  
  def onProcessFinished
    @ui.output.append("<span style='color: green'>#{i18n("Finished")}</span>")
    @ui.button.text = i18n("Close")
    @ui.progressBar.maximum = 1
    @ui.progressBar.value = 1
  end
  
  def stdOut
    @process.setReadChannel(Qt::Process.StandardOutput)
    while @process.canReadLine do
      @ui.output.append("<span>#{@process.readLine}</span>")
    end
  end
  
  def stdErr
    @process.setReadChannel(Qt::Process.StandardError)
    while @process.canReadLine do
      @ui.output.append("<span style='color: red'>#{@process.readLine}</span>")
    end
  end
  
  def onButtonClick
    if @process.pid != 0 then
      @process.kill
      onProcessFinished
    else
      close
    end
  end
end