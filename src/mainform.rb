###########################################################################
#    (c) 2009 Martin Höher (martin@rpdev.net)                             #
#                                                                         #
#    This file is part of KEmu.                                           #
#                                                                         #
#    KEmu is free software: you can redistribute it and/or modify         #
#    it under the terms of the GNU General Public License as published by #
#    the Free Software Foundation, either version 3 of the License, or    #
#    (at your option) any later version.                                  #
#                                                                         #
#    KEmu is distributed in the hope that it will be useful,              #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        #
#    GNU General Public License for more details.                         #
#                                                                         #
#    You should have received a copy of the GNU General Public License    #
#    along with KEmu.  If not, see <http://www.gnu.org/licenses/>.        #
###########################################################################

require 'Qt4'
require 'ui/ui_mainform'

require 'vm'
require 'newimageform'

class MainForm < KDE::XmlGuiWindow
  attr_reader :ui
  attr_reader :newImageForm
  
  def initialize
    super
    widget = Qt::Widget.new
    @ui = Ui::MainForm.new
    @ui.setupUi(widget)
    setCentralWidget(widget)
    setupActions
    createGUI(Dir.getwd + "/kemuui.rc")
    setWindowTitle(i18n("KEmu - Virtual Machine manager"))
    @newImageForm = NewImageForm.new
    resize(600, 400)
    Qt::Application.instance.connect( SIGNAL('aboutToQuit()') ) { saveConfig }
  end
  
  def setupActions
    quitAction = KDE::Action.new(self)
    quitAction.text = i18n("Quit")
    quitAction.icon = KDE::Icon.new("exit")
    quitAction.connect(SIGNAL("triggered(bool)")) { KDE::Application.instance.quit }
    actionCollection.addAction("quit", quitAction)
    
    configureAction = KDE::Action.new(self)
    configureAction.text = i18n("Configure")
    configureAction.icon = KDE::Icon.new("configure")
    configureAction.connect(SIGNAL("triggered(bool)")) { self.configure }
    actionCollection.addAction("configure", configureAction)
    
    deleteAction = KDE::Action.new(self)
    deleteAction.text = i18n("Delete")
    deleteAction.icon = KDE::Icon.new("edit-delete")
    deleteAction.connect(SIGNAL("triggered(bool)")) { self.delete }
    actionCollection.addAction("delete", deleteAction)
    
    deleteAllAction = KDE::Action.new(self)
    deleteAllAction.text = i18n("Delete all")
    deleteAllAction.icon = KDE::Icon.new("edit-clear")
    deleteAllAction.connect(SIGNAL("triggered(bool)")) { self.deleteAll }
    actionCollection.addAction("deleteAll", deleteAllAction)
    
    startAction = KDE::Action.new(self)
    startAction.text = i18n("Start")
    startAction.icon = KDE::Icon.new("arrow-right")
    startAction.connect(SIGNAL("triggered(bool)")) { self.start }
    actionCollection.addAction("start", startAction)
    
    terminateAction = KDE::Action.new(self)
    terminateAction.text = i18n("Terminate")
    terminateAction.icon = KDE::Icon.new("window-close")
    terminateAction.connect(SIGNAL("triggered(bool)")) { self.terminate }
    actionCollection.addAction("terminate", terminateAction)
    
    killAction = KDE::Action.new(self)
    killAction.text = i18n("Kill")
    killAction.connect(SIGNAL("triggered(bool)")) { self.kill }
    actionCollection.addAction("kill", killAction)
    
    outputAction = KDE::Action.new(self)
    outputAction.text = i18n("Show Output")
    outputAction.icon = KDE::Icon.new("utilities-terminal")
    outputAction.connect(SIGNAL("triggered(bool)")) { self.showOutput }
    actionCollection.addAction("showOutput", outputAction)
    
    newAction = KDE::Action.new(self)
    newAction.text = i18n("New")
    newAction.icon = KDE::Icon.new("document-new")
    newAction.connect(SIGNAL("triggered(bool)")) { self.add }
    actionCollection.addAction("new", newAction)
    
    newImageAction = KDE::Action.new(self)
    newImageAction.text = i18n("New Image")
    newImageAction.icon = KDE::Icon.new("archive-insert")
    newImageAction.connect(SIGNAL("triggered(bool)")) { self.newImage }
    actionCollection.addAction("newImage", newImageAction)
  end
  
  def configure
    item = @ui.machineList.currentItem
    if item != nil then
      item.vmachine.configure
    end
  end
  
  def showOutput
    item = @ui.machineList.currentItem
    if item != nil then
      item.vmachine.showOutputWindow
    end
  end
  
  def add(config = nil)
    vm = VMachine.new(i18n('New Virtual Machine'))
    if config != nil then
      vm.loadConfig(config)
    end
    @ui.machineList.addItem(vm.listWidgetItem)
  end
  
  def delete
    if KDE::MessageBox.questionYesNo(nil, 
                                     i18n("Deleting the selected machine cannot be undone. Do you really want to proceed?"),
                                     i18n("Delete Selected Setup")) == 
				     KDE::MessageBox.Yes then
      @ui.machineList.takeItem(@ui.machineList.currentRow)
    end
  end
  
  def deleteAll
    if KDE::MessageBox.questionYesNo(nil,
                                     i18n("Deleting all setups cannot be undone. Do you want to proceed anyway?"),
                                     i18n("Deleting All Setups")) ==
				     KDE::MessageBox.Yes then      
      @ui.machineList.clear
    end
  end
  
  def saveConfig
    config = KDE::Config.new.group("Settings")
    config.writeEntry("numMachines", "#{@ui.machineList.count}")
    (0..@ui.machineList.count-1).each {
      |i| @ui.machineList.item(i).vmachine.saveConfig(config.group("machine#{i}"))
    }                         
    config.sync
  end
  
  def loadConfig
    config = KDE::Config.new.group("Settings")
    numMachines = Integer(config.readEntry("numMachines", "0"))
    (0..numMachines-1).each {
      |i| add(config.group("machine#{i}"))
    }
  end
  
  def start
    item = @ui.machineList.currentItem
    if item != nil then
      if item.vmachine.running then
	KDE::MessageBox.information(nil,
	                            i18n("The virtual machine is already running. Quit it either from inside the guest or terminate or kill it from the actions menu and try starting it then."),
	                            i18n("Virtual Machine is Already Running"))
	return
      end
      item.vmachine.run
    end
  end
  
  def terminate
    item = @ui.machineList.currentItem
    if item != nil and item.vmachine.running then
      if KDE::MessageBox.questionYesNo(nil,
                                       i18n("Terminating a running virtual machine can harm the set up guest. Do you really want to terminate the machine?"),
                                       i18n("Terminate Running Virtual Machine")) ==
				       KDE::MessageBox.Yes then
	item.vmachine.terminate
      end
    end
  end
  
  def kill
    item = @ui.machineList.currentItem
    if item != nil and item.vmachine.running then
      if KDE::MessageBox.questionYesNo(nil,
                                       i18n("Killing a running virtual machine can harm the set up guest. Do you really want to kill the machine?"),
                                       i18n("Kill Running Virtual Machine")) ==
				       KDE::MessageBox.Yes then
	item.vmachine.kill
      end
    end
  end
  
  def newImage
    @newImageForm.show
  end
  
end