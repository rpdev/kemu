install(
    FILES
      ui_mainform.rb
      ui_outputform.rb
      ui_setup_drives.rb
      ui_setup_general.rb
      ui_setup_target.rb
      ui_setup_network.rb
      ui_newimageform.rb
      ui_createimageoutputform.rb
    DESTINATION share/apps/kemu/ui
)
