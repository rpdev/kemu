###########################################################################
#    (c) 2009 Martin Höher (martin@rpdev.net)                             #
#                                                                         #
#    This file is part of KEmu.                                           #
#                                                                         #
#    KEmu is free software: you can redistribute it and/or modify         #
#    it under the terms of the GNU General Public License as published by #
#    the Free Software Foundation, either version 3 of the License, or    #
#    (at your option) any later version.                                  #
#                                                                         #
#    KEmu is distributed in the hope that it will be useful,              #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        #
#    GNU General Public License for more details.                         #
#                                                                         #
#    You should have received a copy of the GNU General Public License    #
#    along with KEmu.  If not, see <http://www.gnu.org/licenses/>.        #
###########################################################################

require 'configform'
require 'vmlistwidgetitem'
require 'outputform'

class VMachine

  attr_reader :configWindow
  attr_reader :outputWindow
  attr_reader :listWidgetItem
  attr_reader :process

  # general
  attr_reader :name
  #drives
  attr_reader :hda
  attr_reader :hdb
  attr_reader :hdc
  attr_reader :hdd
  attr_reader :fda
  attr_reader :fdb
  attr_reader :cdrom
  attr_reader :sd
  attr_reader :obflash
  attr_reader :pflash
  #target
  attr_reader :target
  attr_reader :machine
  attr_reader :bootOn
  attr_reader :cpu
  attr_reader :numCpu
  attr_reader :ram
  attr_reader :soundhw
  attr_reader :allSoundhw
  #usb
  attr_reader :usb
  #graphics
  attr_reader :vgaCard
  attr_reader :noFrame
  attr_reader :noQuit
  attr_reader :portrait
  attr_reader :fullScreen
  attr_reader :altGrab
  #language
  attr_reader :kblayout
  #networking
  attr_reader :userNetwork
  attr_reader :smb
  attr_reader :tftp


  def initialize(name)
    setDefaults
    @name = name
    @configWindow = ConfigForm.new
    @outputWindow = OutputForm.new
    @listWidgetItem = VMListWidgetItem.new(self)
    @configWindow.connect(SIGNAL(:okClicked)) { self.applySettings }
    @process = KDE::Process.new
    @process.setOutputChannelMode(KDE::Process.SeparateChannels)
    @process.connect(SIGNAL(:readyReadStandardOutput)) {self.onProcessOut}
    @process.connect(SIGNAL(:readyReadStandardError)) {self.onProcessErr}
    @process.connect(SIGNAL(:started)) { self.onStarted }
    @process.connect(SIGNAL("finished(int,QProcess::ExitStatus)")) { self.onFinished }
  end

  def setDefaults
    @name = KDE.ki18n("New Virtual Machine")

    @hda = ""
    @hdb = ""
    @hdc = ""
    @hdd = ""
    @fda = ""
    @fdb = ""
    @cdrom = ""
    @sd = ""
    @obflash = ""
    @pflash = ""

    @target = "qemu"
    @machine = ""
    @bootOn = ""
    @cpu = ""
    @numCpu = 1
    @ram = 128
    @soundhw = ""
    @allSoundhw = false

    @usb = true

    @vgaCard = ""
    @noFrame = false
    @noQuit = true
    @portrait = false
    @fullScreen = false
    @altGrab = false

    @kblayout = ""

    @userNetwork = false
    @smb = ""
    @tftp = ""
  end

  def configure

    @configWindow.name = @name

    @configWindow.hda = @hda
    @configWindow.hdb = @hdb
    @configWindow.hdc = @hdc
    @configWindow.hdd = @hdd
    @configWindow.fda = @fda
    @configWindow.fdb = @fdb
    @configWindow.cdrom = @cdrom
    @configWindow.sd = @sd
    @configWindow.obflash = @obflash

    @configWindow.target = @target
    @configWindow.machine = @machine
    @configWindow.bootOn = @bootOn
    @configWindow.cpu = @cpu
    @configWindow.numCpu = @numCpu
    @configWindow.memory = @ram
    @configWindow.soundhw = @soundhw
    @configWindow.allSoundhw = @allSoundhw
    @configWindow.usb = @usb
    @configWindow.vgaCard = @vgaCard
    @configWindow.noFrame = @noFrame
    @configWindow.noQuit = @noQuit
    @configWindow.portrait = @portrait
    @configWindow.fullScreen = @fullScreen
    @configWindow.altGrab = @altGrab

    @configWindow.kblayout = @kblayout

    @configWindow.userNetwork = @userNetwork
    @configWindow.smb = @smb
    @configWindow.tftp = @tftp

    @configWindow.show
  end

  def terminate
    @process.terminate
  end

  def kill
    @process.kill
  end

  def running
    @process.pid != 0
  end

  def applySettings()
    @name = @configWindow.name

    @hda = @configWindow.hda
    @hdb = @configWindow.hdb
    @hdc = @configWindow.hdc
    @hdd = @configWindow.hdd
    @fda = @configWindow.fda
    @fdb = @configWindow.fdb
    @cdrom = @configWindow.cdrom
    @sd = @configWindow.sd
    @obflash = @configWindow.obflash
    @pflash = @configWindow.pflash

    @target = @configWindow.target
    @machine = @configWindow.machine
    @bootOn = @configWindow.bootOn
    @cpu = @configWindow.cpu
    @numCpu = @configWindow.numCpu
    @ram = @configWindow.memory
    @soundhw = @configWindow.soundhw
    @allSoundhw = @configWindow.allSoundhw
    @usb = @configWindow.usb
    @vgaCard = @configWindow.vgaCard
    @noFrame = @configWindow.noFrame
    @noQuit = @configWindow.noQuit
    @portrait = @configWindow.portrait
    @fullScreen = @configWindow.fullScreen
    @fullScreen = @configWindow.altGrab
    @kblayout = @configWindow.kblayout

    @userNetwork = @configWindow.userNetwork
    @smb = @configWindow.smb
    @tftp = @configWindow.tftp

    @listWidgetItem.text = @name
  end

  def saveConfig(config)
    config.writeEntry("name", @name)
    config.writeEntry("hda", @hda)
    config.writeEntry("hdb", @hdb)
    config.writeEntry("hdc", @hdc)
    config.writeEntry("hdd", @hdd)
    config.writeEntry("fda", @fda)
    config.writeEntry("fdb", @fdb)
    config.writeEntry("cdrom", @cdrom)
    config.writeEntry("sd", @sd)
    config.writeEntry("obflash", @obflash)
    config.writeEntry("pflash", @pflash)
    config.writeEntry("target", @target)
    config.writeEntry("machine", @machine)
    config.writeEntry("bootOn", @bootOn)
    config.writeEntry("cpu", @cpu)
    config.writeEntry("numCpu", "#{@numCpu}")
    config.writeEntry("ram", "#{@ram}")
    config.writeEntry("soundhw", @soundhw)
    config.writeEntry("allSoundhw", "#{@allSoundhw}")
    config.writeEntry("usb", "#{@usb}")
    config.writeEntry("vgaCard", @vgaCard)
    config.writeEntry("noFrame", "#{@noFrame}")
    config.writeEntry("noQuit", "#{@noQuit}")
    config.writeEntry("portrait", "#{@portrait}")
    config.writeEntry("fullScreen", "#{@fullScreen}")
    config.writeEntry("altGrab", "#{@altGrab}")
    config.writeEntry("kblayout", @kblayout)
    config.writeEntry("userNetwork", "#{@userNetwork}")
    config.writeEntry("smb", @smb)
    config.writeEntry("tftp", @tftp)
  end

  def loadConfig(config)
    @name = config.readEntry("name", @name)
    @hda = config.readEntry("hda", @hda)
    @hdb = config.readEntry("hdb", @hdb)
    @hdc = config.readEntry("hdc", @hdc)
    @hdd = config.readEntry("hdd", @hdd)
    @fda = config.readEntry("fda", @fda)
    @fdb = config.readEntry("fdb", @fdb)
    @cdrom = config.readEntry("cdrom", @cdrom)
    @sd = config.readEntry("sd", @sd)
    @obflash = config.readEntry("obflash", @obflash)
    @pflash = config.readEntry("pflash", @pflash)
    @target = config.readEntry("target", @target)
    @machine = config.readEntry("machine", @machine)
    @bootOn = config.readEntry("bootOn", @bootOn)
    @cpu = config.readEntry("cpu", @cpu)
    @numCpu = Integer(config.readEntry("numCpu", "#{@numCpu}"))
    @ram = Integer(config.readEntry("ram", "#{@ram}"))
    @soundhw = config.readEntry("soundhw", @soundhw)
    @allSoundhw = config.readEntry("allSoundhw", "#{@allSoundhw}") == "true"
    @usb = config.readEntry("usb", "#{@usb}") == "true"
    @vgaCard = config.readEntry("vgaCard", @vgaCard)
    @noFrame = config.readEntry("noFrame", "#{@noFrame}") == "true"
    @noQuit = config.readEntry("noQuit", "#{@noQuit}") == "true"
    @portrait = config.readEntry("portrait", "#{@portrait}") == "true"
    @fullScreen = config.readEntry("fullScreen", "#{@fullScreen}") == "true"
    @altGrab = config.readEntry("altGrab", "#{@altGrab}") == "true"
    @kblayout = config.readEntry("kblayout", @kblayout)
    @userNetwork = config.readEntry("userNetwork", "#{@userNetwork}") == "true"
    @smb = config.readEntry("smb", @smb)
    @tftp = config.readEntry("tftp", @tftp)

    @listWidgetItem.text = @name
  end

  def run
    @process.clearProgram

    @process << @target

    @process << "-name"
    @process << @name

    if @hda.length > 0
      @process<< "-hda"
      @process<< @hda
    end
    if @hdb.length > 0
      @process<< "-hdb"
      @process<< @hdb
    end
    if @hdc.length > 0 and @cdrom.length == 0
      @process<< "-hdc"
      @process<< @hdc
    end
    if @hdd.length > 0
      @process<< "-hdd"
      @process<< @hdd
    end
    if @cdrom.length > 0
      @process<< "-cdrom"
      @process<< @cdrom
    end
    if @sd.length > 0
      @process<< "-sd"
      @process<< @sd
    end
    if @obflash.length > 0
      @process<< "-mtdblock"
      @process<< @obflash
    end
    if @pflash.length > 0
      @process<< "-pflash"
      @process<< @pflash
    end

    if @machine.length > 0
      @process << "-M"
      @process << @machine
    end
    if @bootOn.length > 0
      @process << "-boot"
      @process << @bootOn
    end
    if @cpu.length > 0
      @process << "-cpu"
      @process << @cpu
    end
    @process << "-smp"
    @process << "#{@numCpu}"
    @process << "-m"
    @process << "#{@ram}"
    if @allSoundhw then
      @process << "-soundhw"
      @process << "all"
    else
      if @soundhw.length > 0
	@process << "-soundhw"
	@process << @soundhw
      end
    end

    if @usb then
      @process << "-usb"
    end

    if @vgaCard.length > 0 then
      @process << "-vga"
      @process << @vgaCard
    end
    if @noFrame then
      @process << "-no-frame"
    end
    if @noQuit then
      @process << "-no-quit"
    end
    if @portrait then
      @process << "-portrait"
    end
    if @fullScreen then
      @process << "-full-screen"
    end
    if @altGrab then
      @process << "-alt-grab"
    end

    if @kblayout.length > 0 then
      @process<< "-k"
      @process<< @kblayout
    end

    if @userNetwork then
      @process << "-net"
      @process << "user"
      @process << "-net"
      @process << "nic"
    end
    if @smb.length > 0 then
      @process << "-smb"
      @process << @smb
    end
    if @tftp.length > 0 then
      @process << "-tftp"
      @process << @tftp
    end

    line = KDE::ki18n("Starting...").toString
    for part in @process.program do
      line = "#{line} #{part}"
    end
    @outputWindow.addInfo(line)
    @process.start
  end

  def onProcessOut
    @process.setReadChannel(Qt::Process.StandardOutput)
    while @process.canReadLine do
      @outputWindow.addStdOut(@process.readLine)
    end
  end

  def onProcessErr
    @process.setReadChannel(Qt::Process.StandardError)
    while @process.canReadLine do
      @outputWindow.addStdErr(@process.readLine)
    end
  end

  def onStarted
    @outputWindow.addInfo(KDE::ki18n("Started successfully!").toString)
    @listWidgetItem.icon = KDE::Icon.new("media-playback-start")
  end

  def onFinished
    @outputWindow.addInfo(KDE::ki18n("Finished successfully.").toString)
    @listWidgetItem.icon = nil
  end

  def showOutputWindow
    @outputWindow.setWindowTitle("#{name} - #{KDE::ki18n("Output").toString}")
    @outputWindow.show
  end
end
