#!/usr/bin/env ruby

=begin

    (c) 2009, 2010 Martin Höher <martin@rpdev.net>

    KEmu - A front end for QEMU and KVM

    KEmu is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    KEmu is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with KEmu.  If not, see <http://www.gnu.org/licenses/>.
    
=end


# Add the parent directory of this file to the LOADPATH: This
# allows us to use relative paths when loading
# other files.

$: << File.dirname( __FILE__ ) + "/../"

# KEmu is KDE software, so load the KDE bindings here once:
require 'korundum4'

require 'gui/MainWindow'

require 'lib/ImageFormat'

about = KDE::AboutData.new( "kemu",                                             # app name
                            "kemu",                                             # catalog name
                            KDE.ki18n( "KEmu" ),                                # display name
                            "0.0.4",                                            # version
                            KDE.ki18n( "Front-end for QEMU and KVM" ),          # short description
                            KDE::AboutData.License_GPL_V3,                      # license
                            KDE::ki18n( "(c) 2009, 2010, RPdev" ),              # copyright statement
                            KDE::ki18n( "http://www.rpdev.net/home/kemu" ) )    # program home page
about.addAuthor(KDE.ki18n( "Martin Höher" ),
                KDE.ki18n( "Lead Programmer, Maintainer" ),
                "martin@rpdev.net",
                "http://www.rpdev.net/home/martin" )
KDE::CmdLineArgs.init(ARGV, about)

KEmu::Emulator.loadEmulators( File.dirname( __FILE__ ) + "/../plugins/emulators/" )
KEmu::VirtualMachineParameter.loadParameters( File.dirname( __FILE__ ) + "/../plugins/parameters/" )
KEmu::ImageFormat.loadImageFormats( File.dirname( __FILE__ ) + "/../plugins/imageformats/" )

app = KDE::UniqueApplication.new
app.setQuitOnLastWindowClosed( false )
mainWindow = KEmu::MainWindow.new
mainWindow.show
exit app.exec
