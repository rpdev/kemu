###########################################################################
#    (c) 2009 Martin Höher (martin@rpdev.net)                             #
#                                                                         #
#    This file is part of KEmu.                                           #
#                                                                         #
#    KEmu is free software: you can redistribute it and/or modify         #
#    it under the terms of the GNU General Public License as published by #
#    the Free Software Foundation, either version 3 of the License, or    #
#    (at your option) any later version.                                  #
#                                                                         #
#    KEmu is distributed in the hope that it will be useful,              #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        #
#    GNU General Public License for more details.                         #
#                                                                         #
#    You should have received a copy of the GNU General Public License    #
#    along with KEmu.  If not, see <http://www.gnu.org/licenses/>.        #
###########################################################################

# Base class for all adapters.
#
# The adapters are used to encapsulate a QEMU executable.
# Different behaviour is partially needed for parsing the command line
# output, as some user selectable values are directly taken from the
# command line output.
class QemuAdapter
    
    # Called once when the adapter is initialized.
    def initialize
        @executables = [:qemu]
    end
    
    # Get the (first) available executable for this target
    #
    # This will go through the internal list of executables and try to call them.
    # Actually, this will call $QEMU_EXE_NAME -version
    # A qemu target will print out a short version notice. If none of the executables
    # could not be found, nil is returned.
    def executable
        for exec in @executables do
            begin
                output = `#{exec} -version`
                if output != nil and output.length > 0 then
                    return exec
                end
            rescue
                #TODO: maybe do some error handling here?
            end
        end
        return nil
    end
    
    # Check if this target is available.
    #
    # Returns whether this target is available.
    def available
        return executable != nil
    end
    
    # Returns name for displaying.
    #
    # Returns a (translated) string ready for beeing displayed.
    def printedName
        return executable
    end
    
    # Check whether a given name matches this targer.
    # 
    # This makes checks against the printed name of this target as
    # well as all assigned executables.
    # Returns true if target matches or false otherwise.
    def matches(target)
        if target == printedName
            return true
        end
        for entry in @executables do
            if target == entry
                return true
            end
        end
        return false
    end
    
    # Get list of available machines.
    #
    # Creates a list of all available machines supported by this target.
    # An empty string is prepended which causes a default machine
    # to be used.
    def machineList
        exec = executable
        if exec != nil then
            output = `#{executable} -M ?`.split("\n")
            output.shift
            for i in (0..output.length-1) do
                output[i] = output[i].split(" ")[0]
            end
            output.unshift ""
            return output
        end
        return [""]
    end
    
    # Get list of available CPUs.
    #
    # Creates a list of all available CPUs for this target and returns it.
    # An empty string is prepended (which causes default CPU to be used when selected).
    def cpuList
        exec = executable
        if exec != nil then
            output = `#{exec} -cpu ?`.split("\n")
            output.unshift ""
            return output
        end
        return [""]
    end

    # Get list of available sound hardware.
    #
    # Creates a list with all available sound hardware for this target.
    # Prepeneds also an empty string to allow default behaviour of the 
    # target to be used.
    def soundHwList
        exec = executable
        if exec != nil then
            output = `#{exec} -soundhw ?`.split("\n")
            output.shift
            list = [""]
            for i in (0..output.length-2) do
                if output[i] != nil and output[i].length > 0 then
                    list.push output[i].split(" ")[0]
                end
            end
            return list
        end
        return [""]
    end
    
end