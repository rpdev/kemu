###########################################################################
#    (c) 2009 Martin Höher (martin@rpdev.net)                             #
#                                                                         #
#    This file is part of KEmu.                                           #
#                                                                         #
#    KEmu is free software: you can redistribute it and/or modify         #
#    it under the terms of the GNU General Public License as published by #
#    the Free Software Foundation, either version 3 of the License, or    #
#    (at your option) any later version.                                  #
#                                                                         #
#    KEmu is distributed in the hope that it will be useful,              #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        #
#    GNU General Public License for more details.                         #
#                                                                         #
#    You should have received a copy of the GNU General Public License    #
#    along with KEmu.  If not, see <http://www.gnu.org/licenses/>.        #
###########################################################################

# Add adapter includes here:
require 'adapters/qemu'
require 'adapters/qemu-system-arm'
require 'adapters/qemu-system-cris'
require 'adapters/qemu-system-m68k'
require 'adapters/qemu-system-mips'
require 'adapters/qemu-system-mips64'
require 'adapters/qemu-system-mips64el'
require 'adapters/qemu-system-mipsel'
require 'adapters/qemu-system-ppc'
require 'adapters/qemu-system-ppc64'
require 'adapters/qemu-system-ppcemb'
require 'adapters/qemu-system-sh4'
require 'adapters/qemu-system-sh4eb'
require 'adapters/qemu-system-sparc'
require 'adapters/qemu-system-x86_64'
require 'adapters/qemu-kvm'

require 'singleton'

# Class holding the list of available adapters.
#
# This class manages the list of all available adapters.
# It is implemented using the Singleton design pattern,
# thus, only one instance is around, which can be accessed via AdapterList.instance.
class AdapterList
    include Singleton
    
    # Initializes the adapter list.
    #
    # Add own adapters here to include them!
    def initialize
        super()
        @adapters = Array.new
        # Add adapters here:
        @adapters.push QemuAdapterDefault.new
        @adapters.push QemuAdapterARM.new
        @adapters.push QemuAdapterCRIS.new
        @adapters.push QemuAdapterM68K.new
        @adapters.push QemuAdapterMIPS.new
        @adapters.push QemuAdapterMIPS64.new
        @adapters.push QemuAdapterMIPS64EL.new
        @adapters.push QemuAdapterMIPSEL.new
        @adapters.push QemuAdapterPPC.new
        @adapters.push QemuAdapterPPC64.new
        @adapters.push QemuAdapterPPCEmb.new
        @adapters.push QemuAdapterSH4.new
        @adapters.push QemuAdapterSH4EB.new
        @adapters.push QemuAdapterSPARC.new
        @adapters.push QemuAdapterX8664.new
        @adapters.push QemuAdapterKVM.new
    end
    
    # Get list of adapters ready for displaying.
    #
    # This builds a list with all available adapters, ready 
    # for beeing displayed.
    def adapterDisplayList
        list = []
        for adapter in @adapters do
            list.push adapter.printedName
        end
        return list
    end
    
    # Find adapter by name.
    # 
    # Looks up whether name matches a registered adapter and - if so -
    # returns that adapter. Otherwise, this method will return nil.
    def findAdapter(name)
        for adapter in @adapters do
            if adapter.matches(name) then
                return adapter
            end
        end
        return nil
    end
end