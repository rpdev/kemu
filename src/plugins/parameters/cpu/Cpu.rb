module KEmu

    module Parameters

        class CpuParameter < KEmu::VirtualMachineParameter

            def self.unique?
                return true
            end
            
            def self.data( role )
                case role
                when Qt::DisplayRole then return Qt::Variant.fromValue( i18n( "CPU" ) )
                when Qt::ToolTipRole then return Qt::Variant.fromValue( i18n( "The emulated CPU." ) )
                end
                return Qt::Variant.new
            end

            attr_accessor :cpu
            
            def initialize( virtualMachine )
                super( virtualMachine )
                clear
                if not virtualMachine.nil? then
                    virtualMachine.connect( SIGNAL('emulatorChanged()') ) { onEmulatorChanged }
                end
            end

            def arguments
                result = []
                result << "-cpu" << "#{ @cpu }" unless @cpu.nil? or @cpu.empty?
                return result
            end

            def saveSettings
                result = Qt::ByteArray.new
                stream = Qt::DataStream.new( result, Qt::IODevice.WriteOnly )
                stream << "#{ @cpu }"
                return result
            end

            def restoreSettings( data )
                clear
                if not data.empty? then
                    stream = Qt::DataStream.new( data, Qt::IODevice.ReadOnly )
                    @cpu = ""
                    stream >> @cpu
                end
            end

            def configurable?
                return true
            end

            def configure
                idx = pickDefaultIndex
                newC = Qt::InputDialog.getItem( nil, i18n( "CPU" ),
                                                  i18n( "Please select a CPU to emulate" ),
                                                  self.cpus.values.sort, idx, false )
                self.cpus.each_pair do | k, v |
                    if v == newC then
                        @cpu = k
                        break
                    end
                end
            end

            def data( role )
                case role
                when Qt::DisplayRole then return Qt::Variant.fromValue( i18n( "CPU: %1" ).gsub( /%1/, "#{ self.cpus[ @cpu ]}" ) )
                else return self.class.data( role )
                end
            end

            protected

            def clear
                @cpu = ""
                self.cpus.each_pair do |k, v|
                    if v =~ /default/
                        @cpu = k
                        break
                    end
                end
            end

            def cpus
                if not self.parent.nil? then
                    if not self.parent.emulator.nil? then
                        return self.parent.emulator.class.cpus
                    end
                end
                return {}
            end

            def pickDefaultIndex
                cpuValues = self.cpus.values.sort
                idx = cpuValues.index( self.cpus[ @cpu ] )
                if not idx.nil? then
                    return idx
                else
                    cpuValues.length.times do | index |
                        if cpuValues[ index ] =~ /default/ then
                            return index
                        end
                    end
                end
                return 0
            end

            def onEmulatorChanged
                @cpu = ""
                self.cpus.each_pair do | k, v |
                    if v =~ /default/ then
                        @cpu = k
                        return
                    end
                end
            end

        end

    end

end