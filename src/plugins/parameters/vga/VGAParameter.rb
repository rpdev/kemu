module KEmu

    module Parameters

        class VGAParameter < KEmu::VirtualMachineParameter

            def self.unique?
                return true
            end

            
            def self.data( role )
                case role
                when Qt::DisplayRole then return Qt::Variant.fromValue( i18n( "VGA Card" ) )
                when Qt::ToolTipRole then return Qt::Variant.fromValue( i18n( "Select the VGA card to emulate." ) )
                end
                return Qt::Variant.new
            end

            attr_accessor :card
            
            def initialize( virtualMachine )
                super( virtualMachine )

                @cards = { "cirrus" => i18n( "Cirrus Logic GD5446 Video card" ),
                           "std" => i18n( "Standard VGA card with Bochs VBE extensions" ),
                           "vmware" => i18n( "VMWare SVGA-II compatible adapter" ),
                           "none" => i18n( "Disable VGA card" )
                         }

                clear
            end

            def arguments
                result = []
                result << "-vga" << @card unless @cards.keys.index( @card ).nil?
                return result
            end

            def saveSettings
                result = Qt::ByteArray.new
                stream = Qt::DataStream.new( result, Qt::IODevice.WriteOnly )
                stream << "#{@card}"
                return result
            end

            def restoreSettings( data )
                clear
                if not data.empty? then
                    stream = Qt::DataStream.new( data, Qt::IODevice.ReadOnly )
                    stream >> @card
                end
            end

            def configurable?
                return true
            end

            def configure
                idx = @cards.keys.index( @card )
                idx |= 0
                labels = @cards.values.sort
                labelIdx = labels.index( @cards[ @card ] )
                labelIdx |= 0
                newCard = Qt::InputDialog.getItem( nil, i18n( "Select VGA Card" ),
                                                      i18n( "Please select a VGA card to use:" ),
                                                      @cards.values.sort, labelIdx, false )
                @cards.keys.each do | key |
                    if @cards[ key ] == newCard then
                        @card = key
                        return
                    end
                end
            end

            def data( role )
                case role
                when Qt::DisplayRole then return Qt::Variant.fromValue( "#{@cards[ @card ]}" )
                else return self.class.data( role )
                end
            end

            private

            def clear
                @card = "cirrus"
            end

        end

    end

end