module KEmu

    module Parameters

        class MachineParameter < KEmu::VirtualMachineParameter

            def self.unique?
                return true
            end
            
            def self.data( role )
                case role
                when Qt::DisplayRole then return Qt::Variant.fromValue( i18n( "Machine" ) )
                when Qt::ToolTipRole then return Qt::Variant.fromValue( i18n( "The emulated machine." ) )
                end
                return Qt::Variant.new
            end

            attr_accessor :machine
            
            def initialize( virtualMachine )
                super( virtualMachine )
                clear
                if not virtualMachine.nil? then
                    virtualMachine.connect( SIGNAL('emulatorChanged()') ) { onEmulatorChanged }
                end
            end

            def arguments
                result = []
                result << "-M" << "#{ @machine }" unless @machine.nil? or @machine.empty?
                return result
            end

            def saveSettings
                result = Qt::ByteArray.new
                stream = Qt::DataStream.new( result, Qt::IODevice.WriteOnly )
                stream << "#{ @machine }"
                return result
            end

            def restoreSettings( data )
                clear
                if not data.empty? then
                    stream = Qt::DataStream.new( data, Qt::IODevice.ReadOnly )
                    @machine = ""
                    stream >> @machine
                end
            end

            def configurable?
                return true
            end

            def configure
                idx = pickDefaultIndex
                newM = Qt::InputDialog.getItem( nil, i18n( "Machine" ),
                                                  i18n( "Please select a machine to emulate" ),
                                                  self.machines.values.sort, idx, false )
                self.machines.each_pair do | k, v |
                    if v == newM then
                        @machine = k
                        break
                    end
                end
            end

            def data( role )
                case role
                when Qt::DisplayRole then return Qt::Variant.fromValue( i18n( "Machine: %1" ).gsub( /%1/, "#{ self.machines[ @machine ]}" ) )
                else return self.class.data( role )
                end
            end

            protected

            def clear
                @machine = ""
                self.machines.each_pair do |k, v|
                    if v =~ /default/
                        @machine = k
                        break
                    end
                end
            end

            def machines
                if not self.parent.nil? then
                    if not self.parent.emulator.nil? then
                        return self.parent.emulator.class.machines
                    end
                end
                return {}
            end

            def pickDefaultIndex
                machineValues = self.machines.values.sort
                idx = machineValues.index( self.machines[ @machine ] )
                if not idx.nil? then
                    return idx
                else
                    machineValues.length.times do | index |
                        if machineValues[ index ] =~ /default/ then
                            return index
                        end
                    end
                end
                return 0
            end

            def onEmulatorChanged
                @machine = ""
                self.machines.each_pair do | k, v |
                    if v =~ /default/ then
                        @machine = k
                        return
                    end
                end
            end

        end

    end

end