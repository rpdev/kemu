module KEmu

    module Parameters

        class TabletParameter < KEmu::VirtualMachineParameter

            def self.unique?
                return true
            end
            
            def self.data( role )
                case role
                when Qt::DisplayRole then return Qt::Variant.fromValue( i18n( "Tablet Mode" ) )
                when Qt::ToolTipRole then return Qt::Variant.fromValue( i18n( "Activates tablet mode: The mouse cursor from the host machine is directly send to the guest, thus emulating a tablet device. This deactivates mouse emulation (and avoids mouse grabbing)." ) )
                end
                return Qt::Variant.new
            end

            def initialize( virtualMachine )
                super( virtualMachine )
            end

            def arguments
                result = []
                result << "-usbdevice" << "tablet"
                return result
            end

            def saveSettings
                result = Qt::ByteArray.new
                return result
            end

            def restoreSettings( data )
            end

            def configurable?
                return false
            end

            def data( role )
                return self.class.data( role )
            end

        end

    end

end