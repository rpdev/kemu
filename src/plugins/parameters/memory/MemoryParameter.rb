module KEmu

    module Parameters

        class MemoryParameter < KEmu::VirtualMachineParameter

            def self.unique?
                return true
            end
            
            def self.data( role )
                case role
                when Qt::DisplayRole then return Qt::Variant.fromValue( i18n( "Memory" ) )
                when Qt::ToolTipRole then return Qt::Variant.fromValue( i18n( "Set the amount of RAM you want to reserve for the virtual machine." ) )
                end
                return Qt::Variant.new
            end

            attr_accessor :memory
            def initialize( virtualMachine )
                super( virtualMachine )

                clear
            end

            def arguments
                result = []
                result << "-m" << "#{ @memory }"
                return result
            end

            def saveSettings
                result = Qt::ByteArray.new
                stream = Qt::DataStream.new( result, Qt::IODevice.WriteOnly )
                stream << "#{ @memory }"
                return result
            end

            def restoreSettings( data )
                clear
                if not data.empty? then
                    stream = Qt::DataStream.new( data, Qt::IODevice.ReadOnly )
                    @memory = ""
                    stream >> @memory
                    @memory = @memory.to_i
                end
            end

            def configurable?
                return true
            end

            def configure
                newMem = Qt::InputDialog.getInt( nil, i18n( "Size of RAM" ), i18n( "Please select the size of the RAM in megabytes" ), @memory, 0 )
                if newMem > 0 then
                    @memory = newMem
                end
            end

            def data( role )
                case role
                when Qt::DisplayRole then return Qt::Variant.fromValue( i18n( "%1 MByte RAM" ).gsub( /%1/, "#{@memory}" ) )
                else return self.class.data( role )
                end
            end

            private

            def clear
                @memory = 128
            end

        end

    end

end