module KEmu

    module Parameters

        class SoundHwParameter < KEmu::VirtualMachineParameter

            def self.unique?
                return true
            end
            
            def self.data( role )
                case role
                when Qt::DisplayRole then return Qt::Variant.fromValue( i18n( "Sound Hardware" ) )
                when Qt::ToolTipRole then return Qt::Variant.fromValue( i18n( "The emulated sound hardware" ) )
                end
                return Qt::Variant.new
            end

            attr_accessor :soundhw
            
            def initialize( virtualMachine )
                super( virtualMachine )
                clear
                if not virtualMachine.nil? then
                    virtualMachine.connect( SIGNAL('emulatorChanged()') ) { onEmulatorChanged }
                end
            end

            def arguments
                result = []
                result << "-soundhw" << "#{ @soundhw }" unless @soundhw.nil? or @soundhw.empty?
                return result
            end

            def saveSettings
                result = Qt::ByteArray.new
                stream = Qt::DataStream.new( result, Qt::IODevice.WriteOnly )
                stream << "#{ @soundhw }"
                return result
            end

            def restoreSettings( data )
                clear
                if not data.empty? then
                    stream = Qt::DataStream.new( data, Qt::IODevice.ReadOnly )
                    @soundhw = ""
                    stream >> @soundhw
                end
            end

            def configurable?
                return true
            end

            def configure
                idx = pickDefaultIndex
                newS = Qt::InputDialog.getItem( nil, i18n( "Sound Hardware" ),
                                                  i18n( "Please select a sound card to emulate" ),
                                                  self.soundhws.values.sort, idx, false )
                self.soundhws.each_pair do | k, v |
                    if v == newS then
                        @soundhw = k
                        break
                    end
                end
            end

            def data( role )
                case role
                when Qt::DisplayRole then return Qt::Variant.fromValue( i18n( "Sound Hardware: %1" ).gsub( /%1/, "#{ self.soundhws[ soundhw ]}" ) )
                else return self.class.data( role )
                end
            end

            protected

            def clear
                @soundhw = ""
                self.soundhws.each_pair do |k, v|
                    if v =~ /default/
                        @soundhw = k
                        break
                    end
                end
            end

            def soundhws
                if not self.parent.nil? then
                    if not self.parent.emulator.nil? then
                        return self.parent.emulator.class.soundhws
                    end
                end
                return {}
            end

            def pickDefaultIndex
                shwValues = self.soundhws.values.sort
                idx = shwValues.index( self.soundhws[ @soundhw ] )
                if not idx.nil? then
                    return idx
                else
                    shwValues.length.times do | index |
                        if shwValues[ index ] =~ /default/ then
                            return index
                        end
                    end
                end
                return 0
            end

            def onEmulatorChanged
                @soundhw = ""
                self.soundhws.each_pair do | k, v |
                    if v =~ /default/ then
                        soundhw = k
                        return
                    end
                end
            end

        end

    end

end