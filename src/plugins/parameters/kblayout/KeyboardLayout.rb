module KEmu

    module Parameters

        class KeyboardLayoutParameter < KEmu::VirtualMachineParameter

            def self.unique?
                return true
            end
            
            def self.data( role )
                case role
                when Qt::DisplayRole then return Qt::Variant.fromValue( i18n( "Keyboard Layout" ) )
                when Qt::ToolTipRole then return Qt::Variant.fromValue( i18n( "Set the keyboard layout." ) )
                end
                return Qt::Variant.new
            end

            attr_accessor :layout
            
            def initialize( virtualMachine )
                super( virtualMachine )

                @layouts = [ "ar", "de-ch", "es", "fo", "fr-ca", "hu", "ja",
                             "mk", "no", "pt-br", "sv", "da", "en-gb", "et",
                             "fr", "fr-ch", "is", "lt", "nl", "pl", "ru", "th",
                             "de", "en-us", "fi", "fr-be", "hr", "it", "lv",
                             "nl-be", "pt", "sl", "tr" ].sort
                clear
            end

            def arguments
                result = []
                result << "-k" << "#{ @layout }" unless @layout.nil? or @layout.empty?
                return result
            end

            def saveSettings
                result = Qt::ByteArray.new
                stream = Qt::DataStream.new( result, Qt::IODevice.WriteOnly )
                stream << "#{ @layout }"
                return result
            end

            def restoreSettings( data )
                clear
                if not data.empty? then
                    stream = Qt::DataStream.new( data, Qt::IODevice.ReadOnly )
                    stream >> @layout
                end
            end

            def configurable?
                return true
            end

            def configure
                idx = @layouts.index( @layout )
                idx ||= 0
                newKBL = Qt::InputDialog.getItem( nil, i18n( "Keyboard Layout" ),
                                                  i18n( "Please select a keyboard layout for the guest" ),
                                                  @layouts, idx, false )
                if not newKBL.nil? and not newKBL.empty? then
                    @layout = newKBL
                end
            end

            def data( role )
                case role
                when Qt::DisplayRole then return Qt::Variant.fromValue( i18n( "Keyboard Layout: %1" ).gsub( /%1/, "#{@layout}" ) )
                else return self.class.data( role )
                end
            end

            private

            def clear
                @layout = @layouts[ 0 ]
            end

        end

    end

end