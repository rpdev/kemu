require 'gui/widgets/URLEdit'

module KEmu
    
    module Parameters

        class StorageDialog < Qt::Dialog

            def initialize( parent = nil )
                super( parent )
                createGui
                setupGui
            end

            def fda
                return @fda.text
            end

            def fda=( img )
                @fda.text = img
            end

            def fdb
                return @fdb.text
            end

            def fdb=( img )
                @fdb.text = img
            end

            def hda
                return @hda.text
            end

            def hda=( img )
                @hda.text = img
            end

            def hdb
                return @hdb.text
            end

            def hdb=( img )
                @hdb.text = img
            end

            def hdc
                return @hdc.text
            end

            def hdc=( img )
                @hdc.text = img
            end

            def hdd
                return @hdd.text
            end

            def hdd=( img )
                @hdd.text = img
            end

            def cdrom
                return @cdrom.text
            end

            def cdrom=( img )
                @cdrom.text = img
            end

            private

            def createGui
                @fdaLabel = Qt::Label.new( i18n( "Floppy Disk A:" ), self )
                @fdbLabel = Qt::Label.new( i18n( "Floppy Disk B:" ), self )

                @hdaLabel = Qt::Label.new( i18n( "Hard Disk A:" ), self )
                @hdbLabel = Qt::Label.new( i18n( "Hard Disk B:" ), self )
                @hdcLabel = Qt::Label.new( i18n( "Hard Disk C:" ), self )
                @hddLabel = Qt::Label.new( i18n( "Hard Disk D:" ), self )

                @cdromLabel = Qt::Label.new( i18n( "CD-ROM:" ), self )

                @fda = Widgets::URLEdit.new( self )
                @fdb = Widgets::URLEdit.new( self )

                @hda = Widgets::URLEdit.new( self )
                @hdb = Widgets::URLEdit.new( self )
                @hdc = Widgets::URLEdit.new( self )
                @hdd = Widgets::URLEdit.new( self )

                @cdrom = Widgets::URLEdit.new( self )

                @buttons = Qt::DialogButtonBox.new( Qt::DialogButtonBox::Ok | Qt::DialogButtonBox::Cancel,
                                                Qt::Horizontal,
                                                self )

                self.layout = Qt::GridLayout.new( self )

                self.layout.addWidget( @fdaLabel, 0, 0 )
                self.layout.addWidget( @fdbLabel, 1, 0 )

                self.layout.addWidget( @hdaLabel, 2, 0 )
                self.layout.addWidget( @hdbLabel, 3, 0 )
                self.layout.addWidget( @hdcLabel, 4, 0 )
                self.layout.addWidget( @hddLabel, 5, 0 )

                self.layout.addWidget( @cdromLabel, 6, 0 )

                self.layout.addWidget( @fda, 0, 1 )
                self.layout.addWidget( @fdb, 1, 1 )

                self.layout.addWidget( @hda, 2, 1 )
                self.layout.addWidget( @hdb, 3, 1 )
                self.layout.addWidget( @hdc, 4, 1 )
                self.layout.addWidget( @hdd, 5, 1 )

                self.layout.addWidget( @cdrom, 6, 1 )

                self.layout.addWidget( @buttons, 8, 0, 1, 2 )
            end

            def setupGui
                @buttons.connect( SIGNAL('accepted()') ) { accept }
                @buttons.connect( SIGNAL('rejected()') ) { reject }
            end

        end

    end

end