require File.dirname( __FILE__ ) + "/StorageDialog"

module KEmu

    module Parameters

        class StorageParameter < KEmu::VirtualMachineParameter

            def self.unique?
                return true
            end
            
            def self.data( role )
                case role
                when Qt::DisplayRole then return Qt::Variant.fromValue( i18n( "Storage Devices" ) )
                when Qt::ToolTipRole then return Qt::Variant.fromValue( i18n( "Select images to use for commonly used devices as floppy disk drives, hard disks or the CR-ROM drive." ) )
                end
                return Qt::Variant.new
            end

            attr_accessor :fda
            attr_accessor :fdb
            attr_accessor :hda
            attr_accessor :hdb
            attr_accessor :hdc
            attr_accessor :hdd
            attr_accessor :cdrom
            
            def initialize( virtualMachine )
                super( virtualMachine )

                clear
            end

            def arguments
                result = []
                result << "-fda"        << @fda     unless @fda.empty?
                result << "-fdb"        << @fdb     unless @fdb.empty?
                result << "-hda"        << @hda     unless @hda.empty?
                result << "-hdb"        << @hdb     unless @hdb.empty?
                result << "-hdc"        << @hdc     unless @hdc.empty?
                result << "-hdd"        << @hdd     unless @hdd.empty?
                result << "-cdrom"      << @cdrom   unless @cdrom.empty?
                return result
            end

            def saveSettings
                result = Qt::ByteArray.new
                stream = Qt::DataStream.new( result, Qt::IODevice.WriteOnly )
                stream << @fda << @fdb
                stream << @hda << @hdb << @hdc << @hdd
                stream << @cdrom
                return result
            end

            def restoreSettings( data )
                clear
                if not data.empty? then
                    stream = Qt::DataStream.new( data, Qt::IODevice.ReadOnly )
                    stream >> @fda >> @fdb
                    stream >> @hda >> @hdb >> @hdc >> @hdd
                    stream >> @cdrom
                end
            end

            def configurable?
                return true
            end

            def configure
                dialog = StorageDialog.new
                dialog.fda = @fda
                dialog.fdb = @fdb
                dialog.hda = @hda
                dialog.hdb = @hdb
                dialog.hdc = @hdc
                dialog.hdd = @hdd
                dialog.cdrom = @cdrom
                if dialog.exec == Qt::Dialog.Accepted then
                    @fda = dialog.fda
                    @fdb = dialog.fdb
                    @hda = dialog.hda
                    @hdb = dialog.hdb
                    @hdc = dialog.hdc
                    @hdd = dialog.hdd
                    @cdrom = dialog.cdrom
                end
                dialog.dispose
            end

            def data( role )
                case role
                when Qt::DisplayRole then return Qt::Variant.fromValue( self.formatCaption() )
                when Qt::ToolTipRole then return Qt::Variant.fromValue( self.formatToolTip() )
                else return self.class.data( role )
                end
            end

            private

            def clear
                @fda = ""
                @fdb = ""
                @hda = ""
                @hdb = ""
                @hdc = ""
                @hdd = ""
                @cdrom = ""
            end

            protected

            def formatToolTip
                result = ""
                result += "\n" + i18n( "Floppy Disk A: #{@fda}" ) unless @fda.empty?
                result += "\n" + i18n( "Floppy Disk B: #{@fdb}" ) unless @fdb.empty?
                result += "\n" + i18n( "Hard Disk A: #{@hda}" ) unless @hda.empty?
                result += "\n" + i18n( "Hard Disk B: #{@hdb}" ) unless @hdb.empty?
                result += "\n" + i18n( "Hard Disk C: #{@hdc}" ) unless @hdc.empty?
                result += "\n" + i18n( "Hard Disk D: #{@hdd}" ) unless @hdd.empty?
                result += "\n" + i18n( "CR-ROM: #{@cdrom}" ) unless @cdrom.empty?

                if result.empty? then
                    result = i18n( "No Storage Devices!" )
                else
                    result = i18n( "Devices:" ) + result
                end
                return result
            end

            def formatCaption
                numDevices = 0
                numDevices += 1 unless @fda.empty?
                numDevices += 1 unless @fdb.empty?
                numDevices += 1 unless @hda.empty?
                numDevices += 1 unless @hdb.empty?
                numDevices += 1 unless @hdc.empty?
                numDevices += 1 unless @hdd.empty?
                numDevices += 1 unless @cdrom.empty?
                if numDevices == 0 then
                    return i18n( "No Storage Devices" )
                elsif numDevices == 1 then
                    return i18n( "1 Storage Device" )
                else
                    return i18n( "%1 Storage Devices" ).gsub( /%1/, "#{numDevices}" )
                end
            end

        end

    end

end