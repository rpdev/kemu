module KEmu

    module Parameters

        class UsbParameter < KEmu::VirtualMachineParameter

            def self.unique?
                return true
            end
            
            def self.data( role )
                case role
                when Qt::DisplayRole then return Qt::Variant.fromValue( i18n( "Enable USB" ) )
                when Qt::ToolTipRole then return Qt::Variant.fromValue( i18n( "Activates USB support in guests." ) )
                end
                return Qt::Variant.new
            end

            def initialize( virtualMachine )
                super( virtualMachine )
            end

            def arguments
                result = []
                result << "-usb"
                return result
            end

            def saveSettings
                result = Qt::ByteArray.new
                return result
            end

            def restoreSettings( data )
            end

            def configurable?
                return false
            end

            def data( role )
                return self.class.data( role )
            end

        end

    end

end