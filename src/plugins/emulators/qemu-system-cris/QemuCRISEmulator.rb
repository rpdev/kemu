module KEmu
    module Emulators
        class QemuCRISEmulator < KEmu::Emulator

            def initialize( virtualMachine )
                super( virtualMachine )
            end

            def executable
                return "qemu-system-cris"
            end

            def mandatoryArguments
                return []
            end

            def self.data( role )
                case role
                when Qt::DisplayRole then return Qt::Variant.fromValue( i18n( "CRIS" ) )
                when Qt::ToolTipRole then return Qt::Variant.fromValue( i18n( "Emulates an ETRAX CRIS embedded system." ) )
                end
                return Qt::Variant.new
            end
            
        end
    end
end