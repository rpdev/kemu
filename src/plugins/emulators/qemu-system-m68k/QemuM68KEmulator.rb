module KEmu
    module Emulators
        class QemuM68KEmulator < KEmu::Emulator

            def initialize( virtualMachine )
                super( virtualMachine )
            end

            def executable
                return "qemu-system-m68k"
            end

            def mandatoryArguments
                return []
            end

            def self.data( role )
                case role
                when Qt::DisplayRole then return Qt::Variant.fromValue( i18n( "Motorola 68000" ) )
                when Qt::ToolTipRole then return Qt::Variant.fromValue( i18n( "Emulates the Motorola 68000 CPU CISC family." ) )
                end
                return Qt::Variant.new
            end

            def self.loadCpus
                output = self.gatherOutput( [ "-cpu", "?" ] )
                output.split( "\n" )
                result = {}
                output.each do | line |
                    line = line.chomp
                    result[ line ] = line
                end
                return result
            end
            
        end
    end
end