install(
    FILES
        init.rb
        QemuM68KEmulator.rb
    DESTINATION
        share/apps/kemu/plugins/emulators/qemu-system-m68k
)
