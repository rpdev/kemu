module KEmu
    module Emulators
        class QemuSH4Emulator < KEmu::Emulator

            def initialize( virtualMachine )
                super( virtualMachine )
            end

            def executable
                return "qemu-system-sh4"
            end

            def mandatoryArguments
                return []
            end

            def self.data( role )
                case role
                when Qt::DisplayRole then return Qt::Variant.fromValue( i18n( "SuperH (SH4)" ) )
                when Qt::ToolTipRole then return Qt::Variant.fromValue( i18n( "32bit RISC architecture." ) )
                end
                return Qt::Variant.new
            end

            def self.loadCpus
                output = self.gatherOutput( [ "-cpu", "?" ] )
                output.split( "\n" )
                result = {}
                output.each do | line |
                    line = line.chomp
                    result[ line ] = line
                end
                return result
            end
            
        end
    end
end