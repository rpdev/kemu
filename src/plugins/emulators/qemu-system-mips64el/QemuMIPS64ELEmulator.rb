module KEmu
    module Emulators
        class QemuMIPS64ELEmulator < KEmu::Emulator

            def initialize( virtualMachine )
                super( virtualMachine )
            end

            def executable
                return "qemu-system-mips64el"
            end

            def mandatoryArguments
                return []
            end

            def self.data( role )
                case role
                when Qt::DisplayRole then return Qt::Variant.fromValue( i18n( "MIPS 64bit (Little Endian)" ) )
                when Qt::ToolTipRole then return Qt::Variant.fromValue( i18n( "64bit RISC architecture using little endian by default." ) )
                end
                return Qt::Variant.new
            end

            def self.loadCpus
                output = self.gatherOutput( [ "-cpu", "?" ] )
                output.split( "\n" )
                result = {}
                output.each do | line |
                    if line =~ /^MIPS '([^']*)'$/ then
                        result[ $1 ] = $1
                    end
                end
                return result
            end
            
        end
    end
end