module KEmu
    module Emulators
        class KVMEmulator < KEmu::Emulator

            def initialize( virtualMachine )
                super( virtualMachine )
                @hasQemuKvm = commandExists?( "qemu-kvm" )
            end

            def executable
                if @hasQemuKvm then
                    return "qemu-kvm"
                else
                    return "qemu"
                end
            end

            def mandatoryArguments
                result = []
                result << "-enable-kvm" unless not @hasQemuKvm
                return result
            end

            def self.data( role )
                case role
                when Qt::DisplayRole then return Qt::Variant.fromValue( i18n( "KVM" ) )
                when Qt::ToolTipRole then return Qt::Variant.fromValue( i18n( "Uses the Intel VT-x or AMD-V CPU extensions to speed up virtualization." ) )
                end
                return Qt::Variant.new
            end

            def self.loadCpus
                output = self.gatherOutput( [ "-cpu", "?" ] )
                output.split( "\n" )
                result = {}
                output.each do | line |
                    if line =~ /x86 *(.*)$/ then
                        result[ $1 ] = $1
                    end
                end
                return result
            end

            private

            def commandExists?(command)
                system("which #{command} > /dev/null 2>/dev/null")
                return false if $?.exitstatus == 127
                return true
            end
            
        end
    end
end