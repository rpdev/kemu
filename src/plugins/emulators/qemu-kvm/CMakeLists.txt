install(
    FILES
        init.rb
        KVMEmulator.rb
    DESTINATION
        share/apps/kemu/plugins/emulators/qemu-kvm
)
