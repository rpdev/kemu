module KEmu
    module Emulators
        class QemuARMEmulator < KEmu::Emulator

            def initialize( virtualMachine )
                super( virtualMachine )
            end

            def executable
                return "qemu-system-arm"
            end

            def mandatoryArguments
                return []
            end

            def self.data( role )
                case role
                when Qt::DisplayRole then return Qt::Variant.fromValue( i18n( "ARM" ) )
                when Qt::ToolTipRole then return Qt::Variant.fromValue( i18n( "Emulates an ARM machine: A 32bit RISC system." ) )
                end
                return Qt::Variant.new
            end

            def self.loadCpus
                output = self.gatherOutput( [ "-cpu", "?" ] )
                output = output.split( "\n" )
                output.shift
                result = {}
                output.each do | line |
                    if line =~ /^ *([^ ]*)/ then
                        result[ $1 ] = $1
                    end
                end
                return result
            end
            
        end
    end
end