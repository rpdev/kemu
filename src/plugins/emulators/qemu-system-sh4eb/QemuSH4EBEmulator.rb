module KEmu
    module Emulators
        class QemuSH4EBEmulator < KEmu::Emulator

            def initialize( virtualMachine )
                super( virtualMachine )
            end

            def executable
                return "qemu-system-sh4eb"
            end

            def mandatoryArguments
                return []
            end

            def self.data( role )
                case role
                when Qt::DisplayRole then return Qt::Variant.fromValue( i18n( "SuperH Embedded (SH4EB)" ) )
                when Qt::ToolTipRole then return Qt::Variant.fromValue( i18n( "Embedded RISC architecture." ) )
                end
                return Qt::Variant.new
            end

            def self.loadCpus
                output = self.gatherOutput( [ "-cpu", "?" ] )
                output.split( "\n" )
                result = {}
                output.each do | line |
                    line = line.chomp
                    result[ line ] = line
                end
                return result
            end
            
        end
    end
end