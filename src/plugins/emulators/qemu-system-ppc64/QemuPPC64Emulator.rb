module KEmu
    module Emulators
        class QemuPPC64Emulator < KEmu::Emulator

            def initialize( virtualMachine )
                super( virtualMachine )
            end

            def executable
                return "qemu-system-ppc64"
            end

            def mandatoryArguments
                return []
            end

            def self.data( role )
                case role
                when Qt::DisplayRole then return Qt::Variant.fromValue( i18n( "PowerPC 64bit" ) )
                when Qt::ToolTipRole then return Qt::Variant.fromValue( i18n( "64bit RISC Architecture." ) )
                end
                return Qt::Variant.new
            end

            def self.loadCpus
                output = self.gatherOutput( [ "-cpu", "?" ] )
                output.split( "\n" )
                result = {}
                output.each do | line |
                    if line =~ /^PowerPC ([^ ]*) *(.*)$/ then
                        result[ $2 ] = $1
                    end
                end
                return result
            end
            
        end
    end
end