module KEmu
    module Emulators
        class QemuSPARCEmulator < KEmu::Emulator

            def initialize( virtualMachine )
                super( virtualMachine )
            end

            def executable
                return "qemu-system-sparc"
            end

            def mandatoryArguments
                return []
            end

            def self.data( role )
                case role
                when Qt::DisplayRole then return Qt::Variant.fromValue( i18n( "Scalable Processor Architecture (SPARC)" ) )
                when Qt::ToolTipRole then return Qt::Variant.fromValue( i18n( "32bit RISC architecture." ) )
                end
                return Qt::Variant.new
            end
            
        end
    end
end