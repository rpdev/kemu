module KEmu
    module Emulators
        class QemuPC64Emulator < KEmu::Emulator

            def initialize( virtualMachine )
                super( virtualMachine )
            end

            def executable
                return "qemu-system-x86_64"
            end

            def mandatoryArguments
                return []
            end

            def self.data( role )
                case role
                when Qt::DisplayRole then return Qt::Variant.fromValue( i18n( "PC (64bit)" ) )
                when Qt::ToolTipRole then return Qt::Variant.fromValue( i18n( "64bit PC emulator." ) )
                end
                return Qt::Variant.new
            end

            def self.loadCpus
                output = self.gatherOutput( [ "-cpu", "?" ] )
                output.split( "\n" )
                result = {}
                output.each do | line |
                    if line =~ /x86 *(.*)$/ then
                        result[ $1 ] = $1
                    end
                end
                return result
            end
            
        end
    end
end