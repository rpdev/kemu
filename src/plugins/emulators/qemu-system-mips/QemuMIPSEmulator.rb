module KEmu
    module Emulators
        class QemuMIPSEmulator < KEmu::Emulator

            def initialize( virtualMachine )
                super( virtualMachine )
            end

            def executable
                return "qemu-system-mips"
            end

            def mandatoryArguments
                return []
            end

            def self.data( role )
                case role
                when Qt::DisplayRole then return Qt::Variant.fromValue( i18n( "MIPS" ) )
                when Qt::ToolTipRole then return Qt::Variant.fromValue( i18n( "32bit RISC architecture" ) )
                end
                return Qt::Variant.new
            end

            def self.loadCpus
                output = self.gatherOutput( [ "-cpu", "?" ] )
                output.split( "\n" )
                result = {}
                output.each do | line |
                    if line =~ /^MIPS '([^']*)'$/ then
                        result[ $1 ] = $1
                    end
                end
                return result
            end
            
        end
    end
end