###########################################################################
#    (c) 2009 Martin Höher (martin@rpdev.net)                             #
#                                                                         #
#    This file is part of KEmu.                                           #
#                                                                         #
#    KEmu is free software: you can redistribute it and/or modify         #
#    it under the terms of the GNU General Public License as published by #
#    the Free Software Foundation, either version 3 of the License, or    #
#    (at your option) any later version.                                  #
#                                                                         #
#    KEmu is distributed in the hope that it will be useful,              #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        #
#    GNU General Public License for more details.                         #
#                                                                         #
#    You should have received a copy of the GNU General Public License    #
#    along with KEmu.  If not, see <http://www.gnu.org/licenses/>.        #
###########################################################################

require 'korundum4'

require 'ui/ui_newimageform'

require 'createimageoutputform'

class NewImageForm < Qt::Widget
  
  attr_reader :ui
  attr_reader :outputForm
  
  def initialize(*k)
    super
    @ui = Ui::NewImageForm.new
    @ui.setupUi(self)
    @ui.fileName.mode = KDE::File.File
    setupActions
    
    @process = KDE::Process.new
    @process.setOutputChannelMode(KDE::Process.SeparateChannels)
    @outputForm = CreateImageOutputForm.new
    @outputForm.process = @process
    @outputForm.setupActions
  end
  
  def setupActions
    @ui.cancelButton.connect(SIGNAL(:clicked)) { self.close }
    @ui.createButton.connect(SIGNAL(:clicked)) { self.createImage }
  end
  
  def createImage
    @process.clearProgram
    @process << "qemu-img"
    @process << "create"
    if @ui.baseFileName.text != "" then
      @process << "-b"
      @process << @ui.baseFileName.text
    end
    @process << "-f"
    @process << @ui.format.currentText
    @process << @ui.fileName.text
    @process << "#{@ui.size.value * 1024}"
    @outputForm.show
    self.close
  end
  
end