#install(
#    FILES
#      configform.rb
#      kemu
#      kemuapp.rb
#      mainform.rb
#      outputform.rb
#      newimageform.rb
#      createimageoutputform.rb
#      vmlistwidgetitem.rb
#      vm.rb
#      kemuui.rc
#    DESTINATION share/apps/kemu
#)

#install(
#    PROGRAMS
#      kemu-manager
#    DESTINATION bin
#)

#add_subdirectory(adapters)
#add_subdirectory(ui)

add_subdirectory(lib)
add_subdirectory(gui)
add_subdirectory(app)
add_subdirectory(bin)
add_subdirectory(icons)
add_subdirectory(plugins)