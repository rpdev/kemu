###########################################################################
#    (c) 2009 Martin Höher (martin@rpdev.net)                             #
#                                                                         #
#    This file is part of KEmu.                                           #
#                                                                         #
#    KEmu is free software: you can redistribute it and/or modify         #
#    it under the terms of the GNU General Public License as published by #
#    the Free Software Foundation, either version 3 of the License, or    #
#    (at your option) any later version.                                  #
#                                                                         #
#    KEmu is distributed in the hope that it will be useful,              #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of       #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        #
#    GNU General Public License for more details.                         #
#                                                                         #
#    You should have received a copy of the GNU General Public License    #
#    along with KEmu.  If not, see <http://www.gnu.org/licenses/>.        #
###########################################################################

require 'Qt4'
require 'korundum4'

require 'ui/ui_outputform'

class OutputForm < Qt::Widget
  
  attr_reader :ui
  
  def initialize(*k)
    super
    @ui = Ui::OutputForm.new
    @ui.setupUi(self)
  end
  
  def addStdOut(line)
    @ui.output.append("<p>#{line}</p>")
  end
  
  def addStdErr(line)
    @ui.output.append("<p style='color: red'>#{line}</p>")
  end
  
  def addInfo(line)
    @ui.output.append("<p style='color: green'>#{line}</p>")
  end
  
  def clear
    @ui.output.clear
  end
end