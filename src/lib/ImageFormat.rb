module KEmu

    # Base class and manager for image formats.
    #
    # This class provides some management methods for image format
    # handling. Additionally, it is the base class for all supported
    # image formats.
    class ImageFormat < Qt::Object

        CHECK          = 0
        CREATE         = 1
        COMMIT         = 2
        CONVERT        = 3
        CONVERT_SRC    = 4
        CONVERT_DST    = 5
        INFO           = 6

        # The qemu-img executable.
        @@executable = "qemu-img"
        
        # The list of registered image formats. Contains classes derived
        # from the ImageFormat class.
        @@imageFormats = []

        # Adds the new image format imageFormat
        # to the list of registered image formats.
        def self.register( imageFormat )
            if not imageFormat.nil? then
                @@imageFormats << imageFormat
            end
        end

        def self.loadImageFormats( path )
            dirs = Dir.glob( "#{path}/*" )
            dirs.each do | dir |
                if File.directory?( dir ) then
                    if File.file?( "#{dir}/init.rb" ) then
                        require "#{dir}/init.rb"
                    end
                end
            end
        end

        # Returns the list of all known image formats.
        def self.imageFormats
            return @@imageFormats
        end

        def self.saveSettings
            result = Qt::ByteArray.new
            stream = Qt::DataStream.new( result, Qt::IODevice.WriteOnly )
            stream << @@executable
            return result
        end

        def self.restoreSettings( data )
            if not data.empty? then
                stream = Qt::DataStream.new( data, Qt::IODevice.ReadOnly )
                @@executable = ""
                stream >> @@executable
            end
        end

        def self.executable
            return @@executable
        end

        def self.executable=(exe)
            @@executable = exe unless exe.nil?
        end

        # Retuns the ouput if qemu-img info for the given image file.
        def self.info( file )
            if File.exists?( file )
                output = `"#{ @@executable }" info "#{ file }"`
                if $? == 0 then
                    return output.split( /\n/ )
                end
            end
            return []
        end

        # Returns the format identifier for the given image.
        #
        # This uses the output of info() for the given image file.
        def self.formatOf( file )
            lines = info( file )
            lines.each do | line |
                if line.match( /^file format: (.*)$/ ) then
                    return $1
                end
            end
            return nil
        end



        
        #########################
        # Interface:
        #########################
        

        # Returns the "name" of the format as used by the qemu-img command
        # line program.
        def formatName
            return nil
        end

        # Returns whether the format supports a given action, e.g. CREATE,
        # CONVERT and so forth.
        def supports( action )
            return false
        end

        # Returns a widget for advanced configuration.
        #
        # Some actions allow to specify additional options
        # depending on the used format. An instance of the image format class
        # will be asked for a widget that allows the user to set these options.
        #
        # Action is the action the widget is created for. Parent is the parent
        # widget (might be nil, in that case, the widget will probably be
        # shown as top-level widget).
        #
        # A format might decide to return nil here; in that case, no
        # user interface will be shown to set special parameters.
        def widgetFor( action, parent = nil )
            return nil
        end

        # Returns the arguments for the given action. Action could be something
        # like CONVERT, CREATE and so forth. The returne arguments should
        # represent the settings the user made in the GUI provided by the
        # widgetFor() method.
        def argumentsFor( action )
            return []
        end

        

    end

end