require 'lib/VirtualMachine'
require 'lib/VirtualMachineModel'

module KEmu

    class VirtualMachineManager < Qt::Object

        signals :machineAdded, 'machineRemoved(int)'

        attr_reader :machines
        attr_reader :model

        def initialize( parent = nil )
            super( parent )
            @machines = []
            @model = VirtualMachineModel.new( self )
        end

        def add( machine )
            if not machine.nil? then
                @machines.push( machine )
                emit machineAdded()
            end
        end

        def remove( machine )
            @machines.length.times do | index |
                if @machines[ index ] == machine then
                    m = @machines.slice!( index )
                    if not m.nil? then
                        m.dispose
                    end
                    emit machineRemoved( index )
                end
            end
        end

        def removeByIndex( index )
            if index >= 0 and index < @machines.length then
                m = @machines.slice!( index )
                if not m.nil? then
                    m.dispose
                end
                emit machineRemoved( index )
            end
        end

        def saveSettings
            result = Qt::ByteArray.new
            stream = Qt::DataStream.new( result, Qt::IODevice.WriteOnly )
            stream << "#{ @machines.length }"
            @machines.each do | machine |
                stream << machine.saveSettings
            end
            return result
        end

        def restoreSettings( data )
            @machines = []
            stream = Qt::DataStream.new( data, Qt::IODevice.ReadOnly )
            numMachines = ""
            stream >> numMachines
            numMachines.to_i.times do
                mdata = Qt::ByteArray.new
                stream >> mdata
                if not mdata.empty? then
                    machine = VirtualMachine.new( self )
                    machine.restoreSettings( mdata )
                    @machines.push( machine )
                end
            end
        end

    end

end