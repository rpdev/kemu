module KEmu

    class VirtualMachineModel < Qt::AbstractItemModel

        def initialize( virtualMachineManager )
            super( virtualMachineManager )
            @parent = virtualMachineManager
            virtualMachineManager.connect( SIGNAL('machineAdded()') ) { onMachineAdded }
            virtualMachineManager.connect( SIGNAL('machineRemoved(int)') ) { | index | onMachineRemoved( index ) }
        end

        def rowCount( index )
            if index.valid? then
                return 0
            else
                return @parent.machines.length
                return 0
            end
        end

        def columnCount( index )
            if index.valid? then
                return 0
            else
                return 2
            end
        end

        def index( row, column, parent )
            return createIndex( row, column )
        end

        def parent( index )
            return Qt::ModelIndex.new
        end

        def data( index, role )
            machine = @parent.machines[ index.row ]
            case index.column
            when 0 then
                if machine then
                    case role
                    when Qt::DisplayRole then return Qt::Variant.fromValue( machine.name )
                    end
                end
            when 1 then
                if not machine.nil? and not machine.emulator.nil? then
                    return machine.emulator.class.data( role )
                end
            end
            return Qt::ModelIndex.new
        end

        def headerData( section, orientation, role )
            if ( orientation == Qt::Horizontal ) then
                case role
                when Qt::DisplayRole then
                    case section
                    when 0 then return Qt::Variant.fromValue( i18n( "Virtual Machine" ) )
                    when 1 then return Qt::Variant.fromValue( i18n( "Emulator" ) )
                    end
                end
            end
            return Qt::ModelIndex.new
        end

        private

        def onMachineAdded
            beginInsertRows( Qt::ModelIndex.new, @parent.machines.length, @parent.machines.length )
            endInsertRows()
        end

        def onMachineRemoved( index )
            beginRemoveRows( Qt::ModelIndex.new, index, index )
            endRemoveRows()
        end

    end

end