module KEmu

    class VirtualMachineParameterModel < Qt::AbstractItemModel

        def initialize( virtualMachine )
            super( virtualMachine )
            @parent = virtualMachine
            virtualMachine.connect( SIGNAL('parameterAdded()') ) { onParameterAdded }
            virtualMachine.connect( SIGNAL('parameterRemoved(int)') ) { | index | onParameterRemoved( index ) }
        end

        def rowCount( index )
            if index.valid? then
                return 0
            else
                return @parent.parameters.length
            end
        end

        def columnCount( index )
            if index.valid? then
                return 0
            else
                return 1
            end
        end

        def index( row, column, parent )
            return createIndex( row, column )
        end

        def parent( index )
            return Qt::ModelIndex.new
        end

        def data( index, role )
            parameter = @parent.parameters[ index.row ]
            if not parameter.nil? then
                return parameter.data( role )
            end
            return Qt::ModelIndex.new
        end

        def headerData( section, orientation, role )
            if ( orientation == Qt::Horizontal ) then
                case role
                when Qt::DisplayRole then
                    case section
                    when 0 then return Qt::Variant.fromValue( i18n( "Parameters" ) )
                    end
                end
            end
            return Qt::ModelIndex.new
        end

        def addParameter( parameter )
            @parent.addParameter( parameter )
        end

        def removeParameter( index )
            @parent.removeParameterByIndex( index.row )
        end

        def virtualMachine
            return @parent
        end

        private

        def onParameterAdded
            beginInsertRows( Qt::ModelIndex.new, @parent.parameters.length, @parent.parameters.length )
            endInsertRows()
        end

        def onParameterRemoved( index )
            beginRemoveRows( Qt::ModelIndex.new, index, index )
            endRemoveRows()
        end

    end

end