module KEmu

    # The Emulator class encapsulates a virtual machine emulator executable,
    # e.g. qemu-system-arm. Specialized classes should implement the dummy
    # methods provided by this class as needed. At least, #executable and
    # #mandatoryArguments should be implemented to provide the executable
    # file and (if required) any mandatory arguments that the emulator needs
    # to run.
    #
    # Additionally, this class provides the API for storing the specialized
    # classes and making them available to other classes. The actual
    # implementations of that class are loaded using a plugin-like mechanism
    # that searches some directories for specially formatted files.
    class Emulator < Qt::Object

        # The array of registered emulator classes
        @@emulators = []

        # A hash holding the machines each emulator can hold. The hash maps the
        # class name of an emulator to a hash of machines it provides.
        @@machines = {}

        # A hash holding the available CPUs for each emulator. It maps the
        # class name of each emulator to a hash, which in turn maps CPU names
        # to labels for displaying.
        @@cpus = {}

        # A hash mapping Emulator class names to hashes which hold mappings
        # from sound hardware cards to a short descriptive label.
        @@soundhws = {}

        # The register method should be used by plugins to add an emulator
        # and make it available. Emulator is the class to be registered.
        # It will be identified by its class name. Additionally, this will
        # invoke some class methods to (once) get lists of available parameters
        # (e.g. the list of available machines per emulator). Each plugin thus
        # must override the required class methods.
        def self.register( emulator )
            if not emulator.nil? then
                @@emulators.push( emulator )
                @@machines[ emulator.name ] = emulator.loadMachines
                @@cpus[ emulator.name ] = emulator.loadCpus
                @@soundhws[ emulator.name ] = emulator.loadSoundHws
            end
        end

        # Creates a emulator instance using the given id, which actually is
        # the class name of an emulator previously registered using #register.
        # Parent is the parent QObject for the emulator to be created.
        def self.create( id, parent = nil )
            @@emulators.each do |emulator|
                if emulator.name == id then
                    return emulator.new( parent )
                end
            end
            return nil
        end

        # Returns the list of all emulators that are currently registered.
        def self.emulators
            return @@emulators
        end

        # Returns a QByteArray with the plugin data.
        def self.saveSettings
            return Qt::ByteArray.new
        end

        # Restore plugin settings from data, which is a QByteArray.
        def self.restoreSettings( data )
        end

        # Returns whether the plugin is configurable.
        def self.configurable?
            return false
        end

        # Configures the plugin.
        def self.configure
        end

        # Load all emulators from the given path.
        #
        # This assumes that in the given path are a set of
        # subdirectories, each with a file called init.rb in it.
        # The method will load that file using require; the containing code thus
        # can register itself in the global emulator plugin storage using the
        # #register method.
        def self.loadEmulators( path )
            dirs = Dir.glob( "#{path}/*" )
            dirs.each do | dir |
                if File.directory?( dir ) then
                    if File.file?( "#{dir}/init.rb" ) then
                        require "#{dir}/init.rb"
                    end
                end
            end
        end


        # Returns data used when displaying lists of available emulators.
        # This is used together with model view classes to provide the data
        # shown to the user in listings.
        def self.data( role )
            return Qt::Variant.new
        end

        # Executes the executable returned by the emulators executable()
        # method with its mandatoryArguments() and the additionalArgs
        # and returns what was printed to stdout.
        #
        # This method is blocking; be careful when using it.
        def self.gatherOutput( additionalArgs = [] )
            begin
                e = self.new( nil )
                exe = e.executable
                args = e.mandatoryArguments
                args += additionalArgs
                args.length.times do | idx |
                    s = args[ idx ].gsub( /"/, "\\\"" ).gsub( / /, "\\ " )
                    args[ idx ] = s
                end
                output = `#{ exe } #{ args.join( " " ) } 2> /dev/null`
                return output
            rescue
                puts "Error executing [#{ exe } #{ args.join( " " ) }}"
            end
            return ""
        end

        # Load the list of available machines (-M parameter) and return it as a
        # hash, mapping each machine to a label (which is used for displaying).
        # This method should be overridden in plugins to do the retrieval.
        # By default, it will use the emulators executable() and
        # mandatoryArguments() methods and call the executable passing 
        # "-M ?" (which causes qemu emulators to print the list if machines).
        # For the majority of emulators, this list can easily be parsed; however,
        # some emulators might want to override this, as their output differs
        # slightly.
        def self.loadMachines
            output = self.gatherOutput( [ "-M", "?" ] )
            output = output.split( "\n" )
            output.shift
            result = {}
            output.each do | line |
                if line =~ /( *)([^ ]*)( *)(.*)/ then
                    result[ $2 ] = $4
                end
            end
            return result
            return {}
        end

        # Load the list of CPUs for the emulator.
        # This method returns a hash, mapping a CPU name to a descriptive
        # label, which can be used in listings.
        # This method will be called once on program startup time for each
        # emulator; the results will be cached.
        #
        # Ideally, emulator plugins should override this method; the default
        # implementation returns only an empty hash!
        def self.loadCpus
            return {}
        end

        # Return the list of available sound hardware
        def self.loadSoundHws
            output = self.gatherOutput( [ "-soundhw", "?" ] ).split( "\n" )
            if output.length == 0 then
                return {}
            end
            output.shift
            output.pop
            output.pop
            result = { 'all' => Qt::Object.i18n( "Enable All Sound Hardware" ) }
            output.each do | line |
                if line =~ /^([^ ]*) *(.*)$/ then
                    result[ $1 ] = $2
                end
            end
            return result
        end

        # Returns the list of machines an emulator can emulate. This will simply
        # do a lookup in a list which is build during program start up.
        # The return value is a hash, mapping a machine to a short label, which
        # is used when displaying the list to the user.
        def self.machines
            result = @@machines[ self.name ]
            if result.nil? then
                return {}
            else
                return result
            end
        end

        # Returns a list of CPUs that can be emulated.
        # The return value is a hash, mapping each CPU to a label which
        # can be used in listings. This methods retrieves the CPU list from
        # a list created on program startup time; it is not supposed to be
        # overridden.
        def self.cpus
            result = @@cpus[ self.name ]
            if result.nil? then
                return {}
            else
                return result
            end
        end

        # Returns a list of available sound hardware devices for the emulator.
        def self.soundhws
            result = @@soundhws[ self.name ]
            if result.nil? then
                return {}
            else
                return result
            end
        end

        # Creates a new Emulator instance. virtualMachine is the
        # VirtualMachine that owns the instance.
        def initialize( virtualMachine = nil )
            super( virtualMachine )
        end

        # Returns the executable for that emulator.
        def executable
            return nil
        end

        # Returns an array with mandatory arguments for the emulator. 
        def mandatoryArguments
            return []
        end

        # Save settings of the plugin instance to a QByteArray.
        def saveSettings
            return Qt::ByteArray.new
        end

        # Restores the settings of a plugin instance from the QByteArray data.
        def restoreSettings( data )
        end

        # Configures the instance.
        def configure
        end

        # Returns true, if the emulator provides a specialized editor
        # for the parameter type identified by parameterId. Returns false
        # otherwise (this will cause the parameter to use its default editor).
        def hasSpecialEditorForParameter?( parameterId )
            return false
        end

        # Edit the VirtualMachineParameter parameter.
        def editParameter( parameter )
        end

    end

end