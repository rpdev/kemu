require 'lib/VirtualMachineParameter'
require 'lib/VirtualMachineParameterModel'
require 'lib/Emulator'

module KEmu

    # Represents a single virtual machine setup, including an emulator and
    # a set of parameters that will be passed to that emulator.
    class VirtualMachine < Qt::Object

        signals :settingsChanged, :emulatorChanged, :parameterAdded, 'parameterRemoved(int)'

        # A name the user can set for the setup. Will be shown in listings.
        attr_reader :name

        # The emulator used for the machine.
        attr_reader :emulator

        # The list of parameters that will be passed to the emulator when
        # it is started.
        attr_reader :parameters

        # The parameter model to be used in GUIs
        attr_reader :parameterModel

        # Creates a new machine setup with parent as the parent object for
        # it.
        def initialize( parent = nil )
            super( parent )
            clear
            @parameterModel = VirtualMachineParameterModel.new( self )
            @runner = nil
        end

        # Saves the settings of the machine to a QByteArray.
        def saveSettings
            result = Qt::ByteArray.new
            stream = Qt::DataStream.new( result, Qt::IODevice.WriteOnly )
            stream << @name
            if @emulator.nil? then
                stream << ""
                emptyData = Qt::ByteArray.new
                stream << emptyData
            else
                stream << @emulator.class.name
                stream << @emulator.saveSettings
            end
            stream << "#{ @parameters.size }"
            @parameters.each do | parameter |
                stream << parameter.class.name
                stream << parameter.saveSettings
            end
            return result
        end

        # Restores the machine from the data which is a QByteArray.
        def restoreSettings( data )
            if not data.empty? then
                clear
                stream = Qt::DataStream.new( data, Qt::IODevice.ReadOnly )
                stream >> @name
                emulatorId = ""
                emulatorData = Qt::ByteArray.new
                stream >> emulatorId >> emulatorData
                @emulator = Emulator.create( emulatorId, self )
                puts "Restoring a " + emulatorId
                if not @emulator.nil? then
                    @emulator.restoreSettings( emulatorData )
                end
                numParams = ""
                stream >> numParams
                numParams = numParams.to_i
                numParams.times do
                    paramId = ""
                    paramData = Qt::ByteArray.new
                    stream >> paramId >> paramData
                    param = VirtualMachineParameter.create( paramId, self )
                    if not param.nil? then
                        param.restoreSettings( paramData )
                        @parameters << param
                    end
                end
            end
        end

        def emulator=( e )
            if not e.nil? then
                @emulator = e
                emit emulatorChanged()
                emit settingsChanged()
            end
        end

        def name=( n )
            if not n.nil? then
                @name = n
                emit settingsChanged()
            end
        end

        def addParameter( p )
            @parameters.push( p )
            emit parameterAdded()
        end

        def removeParameter( p )
            @parameters.size.times do | index |
                if @parameters[ index ] == p then
                    p = @parameters.slice!( index )
                    if not p.nil? then
                        p.dispose
                    end
                    emit parameterRemoved( index )
                    return
                end
            end
        end

        def removeParameterByIndex( i )
            if i >= 0 and i < @parameters.size then
                p = @parameters.slice!( i )
                if not p.nil? then
                    p.dispose
                end
                emit parameterRemoved( i )
            end
        end

        # Returns a list with all parameters matching the given class name.
        def parametersOfClass( className )
            result = []
            @parameters.each do |parameter|
                if parameter.class.name == className then
                    result << parameter
                end unless parameter.nil?
            end
            return result
        end

        def run
            if not running? then
                if @runner.nil? then
                    @runner = Qt::Process.new( self )
                end
                if @emulator.nil? then
                    return false
                end
                arguments = []
                arguments << "--name" << @name
                @emulator.mandatoryArguments.each do | arg |
                    arguments << arg
                end
                @parameters.each do | parameter |
                    args = parameter.arguments
                    args.each do | arg |
                        arguments << arg
                    end
                end
                @runner.start( @emulator.executable, arguments )
            end
            return true
        end

        def running?
            if @runner.nil? then
                return false
            else
                return @runner.state != Qt::Process.NotRunning
            end
        end

        private

        # Resets the machine internally to its defaults.
        def clear
            @emulator = nil
            @parameters = []
            @name = tr( "Virtual Machine" )
        end
        
    end

end