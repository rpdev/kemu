module KEmu

    class VirtualMachineParameterClassModel < Qt::AbstractItemModel

        def initialize( parent = nil )
            super( parent )
        end

        def rowCount( index )
            if index.valid? then
                return 0
            else
                return VirtualMachineParameter.parameters.length
            end
        end

        def columnCount( index )
            if index.valid? then
                return 0
            else
                return 1
            end
        end

        def index( row, column, parent )
            return createIndex( row, column )
        end

        def parent( index )
            return Qt::ModelIndex.new
        end

        def data( index, role )
            parameter = VirtualMachineParameter.parameters[ index.row ]
            if not parameter.nil? then
                return parameter.data( role )
            end
            return Qt::ModelIndex.new
        end

        def headerData( section, orientation, role )
            if ( orientation == Qt::Horizontal ) then
                case role
                when Qt::DisplayRole then
                    case section
                    when 0 then return Qt::Variant.fromValue( i18n( "Parameter" ) )
                    end
                end
            end
            return Qt::ModelIndex.new
        end

    end

end