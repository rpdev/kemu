module KEmu

    # The VirtualMachineParameter class is the base class for parameters that
    # are passed to the emulator.
    #
    # This class provides some dummy methods that specialized classes should
    # implement.
    #
    # Additionally, it provides static storage for plugins to register
    # specialized classes in a plugin-like fashion.
    class VirtualMachineParameter < Qt::Object

        # The list of registered parameter classes 
        @@parameters = []
        
        # The list of registered plugins.
        def self.parameters
            return @@parameters
        end

        # Register a new parameter class.
        # This will add the class parameter to the list of available
        # parameters.
        def self.register( parameter )
            @@parameters.push( parameter ) unless parameter.nil?
        end

        # Returns data about the parameter class. This is used in listings
        # using Qt's model-view approach.
        def self.data( role )
            return Qt::Variant.new
        end

        # Creates a new parameter instance. Id it the class name of the
        # parameter to create. If no such class is registered, nil
        # is returned, otherwise a new parameter instance.
        # virtualMachine is the virtualMachine to which the parameter will
        # belong. 
        def self.create( id, virtualMachine = nil )
            @@parameters.each do | parameter |
                if parameter.name == id then
                    return parameter.new( virtualMachine )
                end
            end
            return nil
        end

        # Returns whether the parameter should be unique per setup.
        # If this returns true, the parameter will be created only once
        # per virtual machine setup. Otherwise, the user will be allowed to
        # create multiple instanced of that parameter.
        def self.unique?
            return false
        end

        # Loads all parameter plugins from the given path.
        #
        # This assumes that in path are some subdirectories, each containing
        # a file called init.rb. This file will then be loaded using require.
        # The containing code can then use this class's register method to
        # add the containging plugin to the list of available parameters.
        def self.loadParameters( path )
            dirs = Dir.glob( "#{path}/*" )
            dirs.each do | dir |
                if File.directory?( dir ) then
                    if File.file?( "#{dir}/init.rb" ) then
                        require "#{dir}/init.rb"
                    end
                end
            end
        end

        # Creates a new instance. virtualMachine is the VirtualMachine to which
        # the parameter will belong. 
        def initialize( virtualMachine = nil )
            super( virtualMachine )
        end

        # Returns the list of arguments as passed to the emulator executable. 
        def arguments
            return []
        end

        # Saves the settings of the parameter to a QByteArray.
        def saveSettings
            return Qt::ByteArray.new
        end

        # Restores the settings from the QByteArray instance data.
        def restoreSettings( data )
        end

        # Returns whether the parameter can be configured. If this returns true,
        # the #configure method can be used to edit the parameter. 
        def configurable?
            return false
        end

        # Configures the parameter. This usually will show a dialog to the
        # user to allow him setting the concrete value of the parameter.
        def configure
        end

        # Returns data about the parameter instance.
        # This is used together with Qt's model-view approach in listings.
        def data( role )
            return Qt::Variant.new
        end
        
    end

end